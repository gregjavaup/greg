package pl.org.pfig.factoryCars;

/**
 * Created by RENT on 2017-06-22.
 */
public class Car {
    private CarsEnum type;
    private String carInfo;
    public CarsEnum getType() {
        return type;
    }

    public void setType(CarsEnum type) {
        this.type = type;
    }

    public String toStringen(){
        return "This car is: " + type.toString() + ". This car info: " + this.carInfo;
    }

    public void setCarInfo(String carInfo) {
        this.carInfo = carInfo;
    }
}
