package pl.org.pfig.factoryCars;

/**
 * Created by RENT on 2017-06-22.
 */
public class CarsFactory {
    private CarsFactory () {}

    public static Car getCar(String car) {
        car.toLowerCase();
        CarsEnum e = null;
        Car c = new Car();
        switch (car) {
            case "bmw":
                e = CarsEnum.BMW;
                c.setCarInfo("BMW is rear wheel drive");
                break;
            case "tir":
                e = CarsEnum.TIR;
                c.setCarInfo("TIR is a semi truck");
                break;
            case "auto":
                e = CarsEnum.AUTO;
                c.setCarInfo("Any normal car");
                break;
            case "truck":
                e = CarsEnum.TRUCK;
                c.setCarInfo("Like a pickup truck");
                break;
        }

        c.setType(e);
        return c;
    }
}
