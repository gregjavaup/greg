package pl.org.pfig.templateMethodUniv;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicYear {
    public void topics() {
        math();
        phisics();
        phsicalEd();
        other();
        System.out.println("");
    }
    public void math() {
        System.out.println("Math");
    }
    public void phisics() {
        System.out.println("Phisics");
    }
    public void phsicalEd() {
        System.out.println("Phisical Education");
    }

   public abstract void other();
}
