package pl.org.pfig.templateMethod;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicComputer {
    public void devices() {
        motherBoard();
        processor();
        externalDevice();
        System.out.println();
    }

    public void motherBoard() {
        System.out.println("Motherboard");
    }

    public void processor() {
        System.out.println("Processor");
    }

    public abstract void externalDevice();
}
