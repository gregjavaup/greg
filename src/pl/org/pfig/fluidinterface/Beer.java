package pl.org.pfig.fluidinterface;

/**
 * Created by RENT on 2017-06-22.
 */
public class Beer {
    private String name;
    private String teaste;
    private String type;
    private double price;

    public Beer() {}

    public String getName() {
        return name;
    }

    public Beer setName(String name) {
        this.name = name;
        return this;
    }

    public String getTeaste() {
        return teaste;
    }

    public Beer setTeaste(String teaste) {
        this.teaste = teaste;
        return this;
    }

    public String getType() {
        return type;
    }

    public Beer setType(String type) {
        this.type = type;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Beer setPrice(double price) {
        this.price = price;
        return this;
    }
    @Override
    public String toString() {
        System.out.println( "Piwo: " + name + ", " + teaste + ", " + type + ", " + price);
        return null;
    }
}
