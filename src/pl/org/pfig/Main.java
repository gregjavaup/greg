package pl.org.pfig;

import pl.org.pfig.factory.AnimalFactory;
import pl.org.pfig.factory.AnimalInterface;
import pl.org.pfig.factoryCars.Car;
import pl.org.pfig.factoryCars.CarsFactory;
import pl.org.pfig.factoryOther.Coupon;
import pl.org.pfig.factoryOther.CouponFactory;
import pl.org.pfig.fluidinterface.Beer;
import pl.org.pfig.fluidinterface.People;
import pl.org.pfig.singleton.KlassMaster;
import pl.org.pfig.templateMethod.Laptop;
import pl.org.pfig.templateMethod.MidiTowerCompuer;
import pl.org.pfig.templateMethod.PersonlComputer;
import pl.org.pfig.templateMethodUniv.Electronic;
import pl.org.pfig.templateMethodUniv.Menagement;
import pl.org.pfig.templateMethodUniv.Person;
import pl.org.pfig.templateMethodUniv.SoftwareEngeneering;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        //singleton
    KlassMaster kl = KlassMaster.getInstance();


    //template method pattern
        Laptop laptop = new Laptop();
        System.out.println("Laptop");
        laptop.devices();

        MidiTowerCompuer mtc = new MidiTowerCompuer();
        System.out.println("MTC");
        mtc.devices();

        PersonlComputer pc = new PersonlComputer();
        System.out.println("PC");
        pc.devices();

        //template method pattern on Univ
        Electronic el = new Electronic();
        System.out.println("Electronic");
        el.topics();

        SoftwareEngeneering sf = new SoftwareEngeneering();
        System.out.println("Software Engeneer");
        sf.topics();

        Menagement mg = new Menagement();
        System.out.println("Menagement");
        mg.topics();

        //Factory pattern
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();

        AnimalInterface dog = AnimalFactory.getAnimal("dog");
        dog.getSound();


        //Factory pattern 2
        int [] numbers = new int[6];
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = new Random().nextInt((48) + 1);
        }

        Coupon c = CouponFactory.getCoupon(numbers);
        System.out.println(c.getA());

        //Factory pattern 3

        Car car = CarsFactory.getCar("bmw");
        System.out.println(car.toStringen());



        //chaining//
        Beer piwo = new Beer();

        piwo
                .setName("Vip")
                .setType("Lager")
                .setTeaste("Shitty")
                .setPrice(1.69)
                .toString();

        // Fluid Interface//
        List<Person> people = new LinkedList<>();

        people.add(new Person("Pawel", "Testowy", 54));
        people.add(new Person("Maciek", "Przykladowy", 12));
        people.add(new Person("Maciek", "Testowy", 34));
        people.add(new Person("Pyrek", "Testowy", 24));

        People pp = new People().addGroup("staff", people);

        for(Person pers : pp.from("staff").latsName("Testowy").name("Pyrek").get()) {
            System.out.println(pers);
        }



    }
}
