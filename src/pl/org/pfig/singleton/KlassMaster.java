package pl.org.pfig.singleton;

/**
 * Created by RENT on 2017-06-22.
 */
public class KlassMaster {
    private static KlassMaster _instance;
    private KlassMaster(){};
    private String name;

    public static KlassMaster getInstance () {
        //lazy
        if(_instance == null) {
            _instance = new KlassMaster();
        }
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}