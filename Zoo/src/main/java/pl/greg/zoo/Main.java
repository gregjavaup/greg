package pl.greg.zoo;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import pl.greg.zoo.dao.AnimalDAO;
import pl.greg.zoo.dao.StuffDAO;
import pl.greg.zoo.entty.Animal;
import pl.greg.zoo.entty.Stuff;
import pl.greg.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by pablojev on 12.07.2017.
 */
public class Main {
/*    public static void main(String[] args) {
      AnimalDAO animalDAO = new AnimalDAO();

        Animal a = new Animal();
        a.setName("Słoń");
        animalDAO.insert(a);
        animalDAO.insert(new Animal ("Ptak"));
        animalDAO.insert(new Animal ("Gołąb"));
        Animal slon = animalDAO.get(1);

        System.out.println("Pobrałeś id = " + slon.getId() + " " + slon.getName());


        slon.setName("PrzerobionySlon");
        animalDAO.update(slon);

        animalDAO.delete(3);

        Animal secondAnimal = new Animal();
        secondAnimal.setId(2);
        animalDAO.delete(secondAnimal);


        for(Animal anim : animalDAO.get()) {
            System.out.println(anim.getId() + ". " + anim.getName());
        }

    }*/

  public static void main(String[] args) {
    StuffDAO stuffDAO = new StuffDAO();

    Stuff a = new Stuff();
    a.setName("Piłka");
    stuffDAO.insert(a);
    stuffDAO.insert(new Stuff ("Patyk"));
    stuffDAO.insert(new Stuff ("Mąka"));
    Stuff slon = stuffDAO.get(1);

    System.out.println("Pobrałeś id = " + slon.getId() + " " + slon.getName());


    slon.setName("PrzerobionySlon");
    stuffDAO.update(slon);

    stuffDAO.delete(3);

    Stuff secondAnimal = new Stuff();
    secondAnimal.setId(2);
    stuffDAO.delete(secondAnimal);


    for(Stuff stuf : stuffDAO.get()) {
      System.out.println(stuf.getId() + ". " + stuf.getName());
    }

  }
}