package pl.greg.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.greg.zoo.entty.Stuff;
import pl.greg.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class StuffDAO implements AbstractDAO<Stuff> {

    public void insert(Stuff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();
    }

    public boolean delete(Stuff type) {
        Session s = HibernateUtil.openSession();
        Transaction t = s.beginTransaction();
        s.delete(type);
        t.commit();
        s.close();
        return true;
    }

    public boolean delete(int id) {
        Session s = HibernateUtil.openSession();
        Transaction t = s.beginTransaction();
        s.delete(this.get(id));
        t.commit();
        s.close();
        return true;
    }

    public boolean update(Stuff type) {
        Session s = HibernateUtil.openSession();
        Transaction t = s.beginTransaction();
        s.update(type);
        t.commit();
        s.close();
        return true;
    }

    public Stuff get(int id) {
        Stuff stuff;
        Session s = HibernateUtil.openSession();
        stuff = s.load(Stuff.class, id);
        s.close();
        return stuff;
    }

    public List<Stuff> get() {
        List<Stuff> stuff;
        Session s = HibernateUtil.openSession();
        stuff = s.createQuery("from Stuff").list();
        s.close();
        return stuff;
    }
}
