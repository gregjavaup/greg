package pl.greg.zoo.entty;


import javax.persistence.*;

/**
 * Created by RENT on 2017-07-12.
 */
@Entity
@Table(name="stuff")
public class Stuff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;
    @Column (name = "age")
    private int age;

    @Column(name="name")
    private String name;

    @Column(name="lastName")
    private String lastName;

    public Stuff() {}

    public Stuff(String Name) {
        this.name = Name;
    }

    public Stuff (int id,String name) {
        this.name = name;
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
