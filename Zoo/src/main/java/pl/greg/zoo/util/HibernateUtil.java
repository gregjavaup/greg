package pl.greg.zoo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by RENT on 2017-07-12.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSesionFactory();

    private static SessionFactory buildSesionFactory() {
        return new Configuration().configure().buildSessionFactory();
    }

    private static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session openSession() {
        return getSessionFactory().openSession();
    }

}
