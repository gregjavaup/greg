package Taby;

public class Main {

	public static void main(String[] args) {
		
		
		DrawWithBuilder dw = new DrawWithBuilder();
		dw.drawSquare(4, 6);
		dw.drawTriangle(4, 6);
		
		

		int[][] ints = new int[6][];

		ints[0] = new int[] { 1, 3, 3 };
		ints[1] = new int[] { 1, 3, 4 };
		ints[2] = new int[] { 1, 3, 5 };
		ints[3] = new int[] { 1, 3, 6 };
		ints[4] = new int[] { 1, 3, 7 };
		ints[5] = new int[] { 1, 3, 8 };
		ints[5] = new int[] { 1, 3, 9 };

		/*int[][][] ints1 = new int[6][][];
		ints1[0][0] = new int[] { 1, 3, 3 };
		ints1[0][1] = new int[] { 1, 3, 4 };
		ints1[0][2] = new int[] { 1, 3, 5 };
*/
		//String a = "";

		//generatesA(a);

		/*
		 * int i = 0; for(int tab : sumaTab(ints)) { System.out.println(tab); }
		 * System.out.println(sumaTab(ints));
		 */
		//generateByBuilder();

	}

	private static void generateByBuilder() {
		StringBuilder bl = new StringBuilder();
		for (int i = 0; i < 3000; i++) {
			bl.append('a');
		}
		System.out.println(bl.toString());
	}
	
	public static void generatesA(String a) {
		for (int i = 0; i < 1000; i++) {
			a += "a";
		}
		System.out.println(a);
	}
	/*
	 * public static int[] sumaTab3(int[][][] liczby) { int[][] results = new
	 * int[liczby.length][liczby.length]; int i = 0; for(int[][] tabs : liczby)
	 * { if(tabs != null) { results[i][i] = sumaTab(tabs); } }
	 * 
	 * return null; }
	 */

	public static int[] sumaTab(int[][] liczby) {
		int[] results = new int[liczby.length];
		int i = 0;
		for (int[] tabs : liczby) {
			if (tabs != null) {
				results[i] = suma(tabs);
			}
			i++;
		}

		return results;
	}

	public static int suma(int[] liczby) {
		int suma = 0;
		for (int i : liczby)
			suma += i;

		return suma;
	}

}
