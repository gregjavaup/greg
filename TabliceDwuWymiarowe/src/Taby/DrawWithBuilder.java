package Taby;

public class DrawWithBuilder {
	
	/*private static int a;
	private static int b;*/

	
	public void drawSquare(int base, int height) {
		StringBuilder build = new StringBuilder();
		for (int j = 0; j <= height; j++) {
			appendLine(base, build);
		}
		System.out.println(build);
	}


	public void appendLine(int base, StringBuilder build) {
		for (int i = 0; i <= base; i++) {
			build.append('*');
		}
		build.append("\n");
	}
	
	
	public void drawTriangle(int base, int height) {
		StringBuilder build = new StringBuilder();
		for (int j = 0; j <= height; j++) {
			appendLine(base, build);
			base--;
		}
		System.out.println(build);
		
	}
	
	/*private static void generateByBuilder() {
		StringBuilder bl = new StringBuilder();
		for (int i = 0; i < 3000; i++) {
			bl.append('a');
		}
		System.out.println(bl.toString());
	}*/

}
