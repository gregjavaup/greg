package Util;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class ScreenRecorder {
    boolean recording = false;
    private Profile profileSeted;
    private List<Profile> profiles_List = new ArrayList<Profile>();


    public void addProfile(Profile profile) {
        profiles_List.add(profile);
    }


    public void setProfile(int nrProfile) {
        nrProfile -= 1;
        try {
            profileSeted = profiles_List.get(nrProfile);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Profile no. " + nrProfile + " Does not exist");
        }
    }

    public void listProfiles() {
        int i = 1;
        for (Profile p : profiles_List) {
            System.out.println("Profile No. " + i + " " + p.toString());
            i++;
        }
    }

    public void startRecording() {
        if (!recording && profileSeted != null) {
            System.out.println("NAGRYWAMY");
            recording = true;
        } else {
            System.out.println("Cannot record! Profile not selected or havent stopped previous recording.");
        }


    }

    public void stopRecording() {
        if (recording) {
            System.out.println("NIE NAGRYWAMY");
        }   else {
        System.out.println("Recording havent started yet");
        }
        recording = false;

    }

}




