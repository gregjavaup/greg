package main;
import Util.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ScreenRecorder screen = new ScreenRecorder();
        menu(screen);
    }

    private static void menu(ScreenRecorder screen) {
        System.out.println("");
        System.out.println("Menu: \n"  +  "Options: add, list, start(record), stop(record), setProfile, exit");
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()) {

            String line = sc.nextLine();

            String[] splits = line.split(" ");

            if(splits[0].equals("add")) {
                screen.addProfile(createProfile());
                System.out.println("Profile added! \n");
            }
            else if(splits[0].equals("list")) {
                screen.listProfiles();
            }
            else if(splits[0].equals("start")) {
                screen.startRecording();
            }
            else if(splits[0].equals("stop")) {
                screen.stopRecording();
            }
            else if(splits[0].equals("setProfile")) {
                System.out.println("Give profile number:");
                int profile = sc.nextInt();
                screen.setProfile(profile);
            }
            else if(splits[0].equals("exit")) {
                return;
            }
            if(!splits[0].equals("")){
            System.out.println("What now?");}
        }
    }

    public static Profile createProfile() {
        Scanner sc = new Scanner(System.in);

        String codecType = addCodec(sc);
        String resolutionType = addResolution(sc);
        String extensionType = addExtention(sc);

        Codec codec = Codec.valueOf(codecType);
        Extension extension = Extension.valueOf(extensionType);
        Resolution resolution = Resolution.valueOf(resolutionType);

        return new Profile(codec, extension, resolution);
    }

    private static String addExtention(Scanner sc) {
        int choise;
        System.out.println("Set extension: ");
        System.out.println("1: " + Extension.avi + " Or 2: " + Extension.mkv + " Or 3: " + Extension.mp3 + " Or 4: "+ Extension.mp4);
        choise = sc.nextInt();
        return choiseExtention(choise);
    }

    private static String addResolution(Scanner sc) {
        int choise;
        System.out.println("Set resoluton: ");
        System.out.println("1: " + Resolution.R800x600 + " Or 2: " + Resolution.R1280x720 + " Or 3 :" + Resolution.R1920x1080 + " Or 4: "+ Resolution.R3840x2160);
        choise = sc.nextInt();
        return choiseResolution(choise);
    }

    private static String addCodec(Scanner sc) {
        int choise;
        System.out.println("Set codec: ");
        System.out.println("1: " + Codec.h263 + " Or 2: " + Codec.h264 + " Or 3: " + Codec.theora + " Or 4: "+ Codec.vr8);
        choise = sc.nextInt();
        return chooseCodec(choise);
    }

    private static String choiseExtention(int choise) {
        if(choise == 1) {
            return "avi";
        } else  if(choise == 2) {
            return "mkv";
        }
        else  if(choise == 3) {
            return "mp3";
        }
        else  if(choise == 4) {
            return "mp4";
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    private static String choiseResolution(int choise) {
        if(choise == 1) {
            return "R800x600";
        } else  if(choise == 2) {
            return "R1280x720";
        }
        else  if(choise == 3) {
            return "R1920x1080";
        }
        else  if(choise == 4) {
            return "R3840x2160";
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    private static String chooseCodec(int choise) {
        if(choise == 1) {
            return "h263";
        } else  if(choise == 2) {
            return "h264";
        }
        else  if(choise == 3) {
            return "theora";
        }
        else  if(choise == 4) {
            return "vr8";
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
