package Tests;
import Queues.ArrayQueueList;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-06-27.
 */
public class QueueTest {
    ArrayQueueList eq = new ArrayQueueList();
    ArrayQueueList underTest;

    @Test
    public void offer() throws Exception {
        eq.offer(0);
        eq.offer(4);
        eq.offer(1);
        eq.offer(2);
        eq.offer(0);
        eq.offer(5);
        eq.offer(6);
    }
    @Test
    public void offerAnPoll3Elements() {
        underTest = new ArrayQueueList();
        underTest.offer(2);
        underTest.offer(4);
        underTest.offer(6);

        assertThat(underTest.isEmpty()).isFalse();
        assertThat(underTest.poll()).isEqualTo(2);
        assertThat(underTest.poll()).isEqualTo(4);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerPeekAndPool2Times() {
        underTest = new ArrayQueueList();
        underTest.offer(2);
        underTest.peek();
        underTest.poll();
        underTest.offer(6);

        assertThat(underTest.peek()).isEqualTo(6);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements() {
        underTest = new ArrayQueueList();
        for (int i = 0; i < 100; i++) {
            underTest.offer(i);
        }

        for (int i = 0; i < 100; i++) {
            assertThat(underTest.poll()).isEqualTo(i);
        }
    }

}