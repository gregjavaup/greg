package Queues;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class LinkedQueueList implements QueueInterface {
    private Element first;
    private Element last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int value) {
        Element element = new Element(value);
        if(isEmpty()) {
            first = element;
        } else {
            last.next = element;
        }
        last = element;
    }

    @Override
    public int poll() {
        int result = peek();
        first = first.next;
        if(first == null) {
            last = null;
        }
        return result;
    }

    @Override
    public int peek() {
        if(!isEmpty()) {
           return first.value;
        }else {
            throw new NoSuchElementException();
        }
    }

    private static class Element {
        int value;
        Element next;
        public Element(int value) {
            this.value = value;
        }
    }
}
