package Queues.generic;

/**
 * Created by RENT on 2017-06-27.
 */
public interface QueueInterface<T> {
    boolean isEmpty();
    void offer(T value);
    T poll();
    T peek();
}
