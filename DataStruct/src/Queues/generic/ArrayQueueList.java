package Queues.generic;


import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class ArrayQueueList<T> implements QueueInterface<T> {
    private Object[] data;
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    public ArrayQueueList() {
        this(8);
    }

    public ArrayQueueList(int size) {
        data = new Object[size];
    }

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(T value) {
        if (isFull()) {
            createNewArray();
        }

        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    private void createNewArray() {
        Object[] newData = new Object[data.length*2];
        int i = 0;
        while (!isEmpty) {
            newData[i] = poll();
            i++;
        }

        data = newData;
        start = 0;
        end = i;
    }

    private boolean isFull() {
        return start == end && !isEmpty;
    }

    @Override
    public T poll() {
        if (!isEmpty) {
            T result = (T) data[start];
            data[start] = null;
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public T peek() {
        if (!isEmpty) {
            return (T) data[start];
        }else {
            throw new NoSuchElementException();
        }
    }
}
