package Queues;

/**
 * Created by RENT on 2017-06-27.
 */
public interface QueueInterface {
    boolean isEmpty();
    void offer(int value);
    int poll();
    int peek();
}
