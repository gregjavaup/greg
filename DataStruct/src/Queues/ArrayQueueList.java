package Queues;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class ArrayQueueList implements QueueInterface {
    int size = 5;
    int[] tab = new int [size];
    int start;
    int end;
    boolean isEmpty = true;


    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if(start - 1 == end || (end == start && isEmpty == false)) {
            int startHelp = start;
            int[] helpTab;
            helpTab = new int[size * 2];
            for (int i = 0; i < size - start; i++) {
                if(start + i >= size) {
                    helpTab[i] = tab[startHelp];
                    startHelp++;
                    end ++;
                } else {
                    helpTab[i] = tab[start + i];
                    end ++;
                }
            }
            tab = helpTab;
            size *= 2;
            start = 0;
        }
        tab[end] = value;
        end = (end+1) % tab.length;
        isEmpty = false;

    }

    @Override
    public int poll() {
        if (!isEmpty) {
            int result = tab[start];
            start = (start + 1) % tab.length;
            if (start == end) {
                isEmpty = true;
            }
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int peek() {
        if (!isEmpty) {
            return tab[start];
        }else {
            throw new NoSuchElementException();
        }
    }
}
