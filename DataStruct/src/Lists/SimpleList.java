package Lists;

/**
 * Created by RENT on 2017-06-26.
 */
public interface SimpleList {
    void add(int value);
    void add(int value,int index);
    boolean contain(int value);
    void remove(int index);
    void removeValue(int index);
    int size();
}
