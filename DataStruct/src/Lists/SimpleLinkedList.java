package Lists;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedList implements SimpleList {

    private Element first;
    private Element last;
    private int number;

    @Override
    public void add(int value) {
        Element e = new Element(value);
        if (number > 0) {
            e.prev = last;
            last.next = e;
        } else {
            first = e;
        }
        last = e;
        number++;
    }

    @Override
    public void add(int value, int index) {
        if(index < number) {
            Element e = new Element(value);
            Element b = IterateToElement(index);
            e.next = b;
            e.prev = b.prev;
            b.prev.next = e;
            b.prev = e;
            number++;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }



    @Override
    public boolean contain(int valueOf) {
        Element b =  first;
        while(b != null) {
            if(b.value == valueOf) {
                return true;
            }
            b =  b.next;
        }


        return false;
    }

    @Override
    public void remove(int index) {
        if(index > number) {
            throw new IndexOutOfBoundsException();
        }

        Element b = IterateToElement(index);
        if(index < number-1 && index != 0) {
            b.prev.next = b.next;
            b.next.prev = b.prev;
        } else if(number == 1) {
            first = null;
            last = null;
        }else if (index == number-1) {
            last = b.prev;
            last.next = null;
        } else if(index == 0) {
            first = b.next;
            first.prev = null;
        }
    number--;

    }

    @Override
    public void removeValue(int valueOf) {
        Element b =  first;
        for(int i = 0; i < number ; i++) {
            if(b.value == valueOf) {
                remove(i);
            }
            b =  b.next;
        }


    }

    public int get (int index) {
        if(index <= number) {
            Element b = IterateToElement(index);
            return b.value;
        } else {
            throw  new IndexOutOfBoundsException();
        }
    }

    @Override
    public int size() {
        return number;
    }

    private Element IterateToElement(int index) {
        Element b =  first;
        for(int i = 0; i < index ; i++) {
            b =  b.next;
        }
        return b;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;

        public Element(int value) {
            this.value = value;
        }
    }

}
