package Lists;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleArrayList implements SimpleList {

    int number = 0;
    int[] data = new int[100];

    public void add(int value) {
        if (number < data.length) {
            data[number] = value;
            number++;
        } else {
            int[] newData = new int[data.length*2];
            for(int i = 0 ; i < number ; i++){
                newData[i]= data[i];
            }
            data = newData;
            add(value);

        }
    }

    public void add(int value, int index) {
        if (number < data.length) {
            if (index < number) {
                for (int i = number; i > index; i--) {
                    data[i] = data[i - 1];
                }
                data[index] = value;
            } else {
                data[number] = value;
            }
            number++;
        } else throw new IllegalArgumentException();
    }

    public int get(int index) throws ArrayIndexOutOfBoundsException {
        if (index >= number || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[index];
    }

    public boolean contain(int value) {
        for (int i = 0; i < number; i++) {
            if (data[i] == value) {
                return true;
            }
        }
        return false;
    }

    public void remove(int index) {
        if (index < number && index >= 0) {
            for (int i = index; i < number; i++) {
                data[i] = data[i + 1];
            }
            number--;
        } else {
            throw new IllegalArgumentException("Zły indeks");
        }
    }

    public void removeValue(int value) {
        for (int i = 0; i < number; i++) {
            if (data[i] == value) {
                remove(i);
                number--;
                break;
            }
        }
    }

    public int size() {
        return number;
    }
}
