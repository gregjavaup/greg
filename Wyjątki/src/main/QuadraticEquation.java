package main;

import java.util.Scanner;

public class QuadraticEquation {
	private int a = 0;
	private int b = 0;
	private int c = 0;

	public QuadraticEquation() {
		super();
	}

	public QuadraticEquation(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void setA(int a) {
		this.a = a;
	}

	public void setB(int b) {
		this.b = b;
	}

	public void setC(int c) {
		this.c = c;
	}
	
	public void setAll() {
		if(a == 0 && b == 0 && c == 0) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Podaj a: "); 
			a = sc.nextInt();
			System.out.println("Podaj b: "); 
			b = sc.nextInt();
			System.out.println("Podaj c: "); 
			c = sc.nextInt();
			sc.close();
		}
	}

	/*public void setIndyvidual() {
		if (a == 0)
			setA(a);
		if (b == 0)
			setA(b);
		if (c == 0)
			setA(c);
	}*/

	public double[] solve() throws ArithmeticException {
		double[] tab = new double[2];
		setAll();
		
		double x1, x2, delta;
		delta = b*b -4*a*c;
		if(delta >= 0) {
		x1= (-b - Math.sqrt(delta)) / (2 * a);
		x2= (-b + Math.sqrt(delta)) / (2 * a); }
		
		else {
			throw new ArithmeticException("Delta ujemna");
		}
		return new double[] {x1, x2};
	}

	

}
