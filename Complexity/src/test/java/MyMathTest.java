import org.junit.Test;


import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMathTest {
    MyMath ma = new MyMath();

    @org.junit.Test
    public void sum() throws Exception {
        assert ma.sum(3,4) == 7;
    }

    @Test
    public void power() {
        assert ma.pow(2,3) == 8;
    }

}