import java.util.Random;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMath {

    public static int sum(int a, int b) { // O(1)
        return a+b;
    }
    public static long pow(int a, int b) { // O(n)
        for(int i = 0; i < b; i++) {
            a*=a;
        }
        return a;
    }
    public static int[] randArray(int size) { // O(n)
        int[] result = new int[size];
        Random random = new Random();
            for(int i = 0; i < size; i++) {
                result[i] = random.nextInt();
        }
        return result;
    }

    public static int[][] randArrayTwoDem(int size) { // O(n^2)
        int[][] result = new int[size][];
        for(int i = 0; i < size; i++) {
            result[i] = randArray(size);
        }
        return result;
    }

    public static int find(int[] data, int what) {
        int a = 0;
        int b = data.length;


        while(a<=b) {
            int s = (a+b) /2;

            if(what <= data[s]) {
                b = s;
            } else {
                a = s;
            }
        }

        if (data[a] == what) {
            return a;
        }
        return -1;
    }

}
