package pl.org.pfig.tdd;

public class Exercises {

	public int NWD(int a, int b) {
		if ((a == 0 && b != 0) || (b == 0 && a != 0)) {
			throw new ArithmeticException("");
		}

		while (a != b) {
			if (a > b) {
				a -= b;
			} else {
				b -= a;
			}
		}

		return a;
	}

	public int[] processNumbers(int a, int b) {
		int[] results = new int[b - a + 1];
		if ((a < 0 || a > 256) || (b < 0 || b > 256) || a > b) {
			throw new IllegalArgumentException();
		}

		for (int i = a, end = b - a; i <= b; i++) {
			if (i % 2 == 0) {
				results[i] = i;
			} else {
				results[end] = i;
				end--;
			}
		}
		return results;
	}

}