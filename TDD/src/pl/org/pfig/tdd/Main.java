package pl.org.pfig.tdd;

import static org.junit.Assert.assertEquals;

public class Main {

	public static void main(String[] args) {
		Exercises e = new Exercises();
		int[][] dataSet = { 
				{48, 12, 12},
				{210, 6, 6},
				{-9, 3, 3},
				{21, -7, 7},
				{-36, -6, 6}
		};
		for(int[] data : dataSet) {
			 e.NWD(data[0], data[1]);
		}
		
		int[] i = e.processNumbers(3, 8);
		for(int d : i) {
			System.out.println(d);
		}
		
	}

}
