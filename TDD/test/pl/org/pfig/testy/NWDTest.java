package pl.org.pfig.testy;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.tdd.Exercises;

public class NWDTest {
	Exercises e;
	
	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenGivenRandomDataExpectProperResult() {	
		int[][] dataSet = { 
				{48, 12, 12},
				{210, 6, 6},
				{-9, 3, 3},
				{21, -7, 7},
				{-36, -6, 6}
		};
		
		for(int[] data : dataSet) {
			assertEquals(data[2], e.NWD(data[0], data[1]));
		}
	}
	
	@Test(expected = Exception.class)
	public void whenGivenWrongDataExpectException() {
		int[][] dataSet = { 
				{48, 0},
				{0, 6},
		};
		for(int[] data : dataSet) {
			e.NWD(data[0], data[1]);
		}
		
	}
	

}
