package pl.projekt.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.List;

@Entity

public class Brand {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;

    @JsonBackReference
    @OneToMany(mappedBy = "brand")
    private List<Auction> auctions;

   // @JsonManagedReference
    @JsonIgnore
    @OneToMany(mappedBy = "brand")
    private List<Model> models;


    public Brand() {
    }

    public Brand(String name, List<Auction> auctions, List<Model> models) {
        this.name = name;
        this.auctions = auctions;
        this.models = models;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public List<Model> getModels() {return models; }

    public void setModels(List<Model> models) {this.models = models;}

}
