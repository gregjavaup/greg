package pl.projekt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class Auction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    private String content;
    private String title;
    private String image;
    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Brand brand;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Model model;

    public Auction() {
    }

    public Auction(String content, String title, String image, Brand brand, Model model) {
        this.content = content;
        this.title = title;
        this.image = image;
        this.brand = brand;
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}
