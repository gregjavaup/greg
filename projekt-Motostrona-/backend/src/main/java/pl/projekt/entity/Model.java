package pl.projekt.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Model {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;

//    @JsonBackReference
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private Brand brand;

    @JsonBackReference
    @OneToMany(mappedBy = "model")
    private List<Auction> auctions;

    public Model() {
    }

    public Model(String name, Brand brand, List<Auction> auctions) {
        this.name = name;
        this.brand = brand;
        this.auctions = auctions;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    // TESTOWA ZMIANA
}
