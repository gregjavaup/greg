package pl.projekt.repository;

import org.springframework.data.repository.CrudRepository;
import pl.projekt.entity.Auction;

public interface AuctionRepository extends CrudRepository<Auction, Long> {
}
