package pl.projekt.repository;

import org.springframework.data.repository.CrudRepository;
import pl.projekt.entity.Model;

public interface ModelRepository extends CrudRepository<Model, Long> {
}
