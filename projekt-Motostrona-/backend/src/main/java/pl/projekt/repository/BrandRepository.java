package pl.projekt.repository;

import org.springframework.data.repository.CrudRepository;
import pl.projekt.entity.Brand;

public interface BrandRepository extends CrudRepository<Brand, Long> {
}
