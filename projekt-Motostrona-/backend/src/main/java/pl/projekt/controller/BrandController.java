package pl.projekt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.projekt.entity.Brand;
import pl.projekt.entity.Model;
import pl.projekt.repository.BrandRepository;
import pl.projekt.repository.ModelRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/brands")
public class BrandController {

    @Autowired
    private BrandRepository brandRepository;

    @RequestMapping("/add")
    public Brand add(@RequestParam(name = "name") String name) {
        Brand b = new Brand();
        b.setName(name);
        return brandRepository.save(b);
    }

    @RequestMapping("/show")
    public List<Brand> showAll() {return (List<Brand>) brandRepository.findAll();}

    @RequestMapping("/show/{id}")
    public Brand showById(@PathVariable(name = "id") String id) {
        return brandRepository.findOne(Long.valueOf(id));
    }

}

