package pl.projekt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.projekt.entity.Brand;
import pl.projekt.entity.Model;
import pl.projekt.repository.BrandRepository;
import pl.projekt.repository.ModelRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/models")
public class ModelController {

    @Autowired
    private ModelRepository modelRepository;

    @Autowired
    private BrandRepository brandRepository;

    @RequestMapping("/add")
    public Model add(@RequestParam(name = "name") String model_name,
                     @RequestParam(name = "brand") String brand) {

        Brand b = brandRepository.findOne(Long.valueOf(brand));
        Model m = new Model();
        m.setName(model_name);
        m.setBrand(b);
        return modelRepository.save(m);
    }

    @RequestMapping("/show")
    public List<Model> showAll() {return (List<Model>) modelRepository.findAll();}

    @RequestMapping("/show/{id}")
    public Model showById(@PathVariable(name = "id") String id) {
        return modelRepository.findOne(Long.valueOf(id));
    }


}
