package pl.projekt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.projekt.entity.Auction;
import pl.projekt.entity.Brand;
import pl.projekt.entity.Model;
import pl.projekt.repository.AuctionRepository;
import pl.projekt.repository.BrandRepository;
import pl.projekt.repository.ModelRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/auctions")
public class AuctionController {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private ModelRepository modelRepository;

    @RequestMapping("/")
    public String auction() {return "";}

    @RequestMapping("/show")
    public List<Auction> listAuctions() {return (List<Auction>) auctionRepository.findAll();}

    @RequestMapping("/show/{id}")
    public Auction showAuctionByID(@PathVariable("id") String id) {
        return auctionRepository.findOne(Long.valueOf(id));
    }

    @RequestMapping("/add/{id}")
    public Auction addAuction(/*@PathVariable("id") String id,*/
                              @RequestParam(name = "content", defaultValue = "")String content,
                              @RequestParam(name = "title", defaultValue = "")String title,
                              @RequestParam(name = "image", defaultValue = "") String image,
                              @RequestParam(name = "brand") String brand,
                              @RequestParam(name = "model") String model) {
        /*long myId = Long.valueOf(id);*/
        Brand b = brandRepository.findOne(Long.valueOf(brand));
        Model m = modelRepository.findOne(Long.valueOf(model));
        Auction a = new Auction();
        /*a.setId(myId);*/
        a.setTitle(title);
        a.setContent(content);
        a.setImage(image);
        a.setBrand(b);
        a.setModel(m);
        return auctionRepository.save(a);
    }
    @RequestMapping("/delete/{id}")
    public boolean deleteAnimal(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        Auction a = auctionRepository.findOne(myId);
        a.setBrand(null);
        a.setModel(null);
        auctionRepository.delete(a);
        return true;
    }
}
