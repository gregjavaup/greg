var app = angular.module('MotostronaApp', ['ngRoute']);
var url = 'http://localhost:8088/';

/*RUTERY*/
app.config(function($routeProvider) {
    var path = './vievs/';
    $routeProvider
    .when('/main', {
        templateUrl: path + 'main.html',
        controller: 'showAuctionsController'
    })
    .when('/main/:id', {
        templateUrl: path + 'auction.html',
        controller: 'auctionController'
    })
    .when('/add/:id', {
          templateUrl: path + 'add.html',
          controller: 'addController'
    }).when('/delete/:id', {
            templateUrl: path + 'delete.html',
            controller: 'deleteController'
    }).when('/brands', {
        templateUrl: path + 'brands.html',
        controller: 'brandsController'
    }).when('/brand/:id', {
        templateUrl: path + 'brand.html',
        controller: 'brandController'
    });
});


app.controller('addController', function($scope, $http,  $routeParams) {
    $http({
        url: url + 'brands/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.brands = success.data;
    }, function(error) {
        console.error(error);
    });
    
     $http({
        url: url + 'models/show',
        dataType: 'json'
    }).then(function(success){
        $scope.models = success.data;
    }, function(error){
        console.error(error);
    });
    
    var id = $routeParams.id;
    $scope.add = function() {
        $http({
            url: url + 'auctions/add/' + id,
            dataType: 'json',
            params: {
                content: $scope.content,
                title: $scope.title,
                image: $scope.image,
                brand: $scope.brand,
                model: $scope.model
            }
        }).then(function(success){
            console.log(success);
            $scope.message = "Dodano pomyślnie aukcje.";
        }, function(error) {
            console.error(error);
        });
    }
})

app.controller('deleteController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'auctions/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Usunięto poprawnie.";
    }, function(err) {
        console.error(err);
    });
});

app.controller('brandsController', function($scope, $http) {
    $http({
        url: url + 'brands/show',
        dataType: 'json'
    }).then(function(success){
        $scope.brands = success.data;
    }, function(error){
        console.error(error);
    });
    
    $scope.add = function() {
        $http({
            url: url + 'brands/add',
            dataType: 'json',
            params: {
                name: $scope.name,
            }
        }).then(function(success) {
            if(success.data.id > 0) {
            $scope.brands.push(success.data);
            $scope.message = "Dodano poprawnie"
            } else {
                $scope.message = "Wystąpił błąd"
            }
        }, function(error) {
            console.error(error);
        });
    }
})


app.controller('brandController', function($scope, $http, $routeParams) { 
    $scope.models = [];
    var id = $routeParams.id;
     $http({
        url: url + 'brands/show/' + id,
        dataType: 'json'
    }).then(function(success){
        $scope.brand = success.data;
    }, function(error){
        console.error(error);
    });

    
    $http({
        url: url + 'models/show',
        dataType: 'json'
    }).then(function(success){
        $scope.models = success.data;
    }, function(error){
        console.error(error);
    });

    $scope.add = function() {
        $http({
            url: url + 'models/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                brand: id
            }
        }).then(function(success) {
            console.log(success);
            $scope.models.push(success.data);
            $scope.message = "Dodano poprawnie.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showAuctionsController', function($scope, $http) 
{
    $http({
        url: url + 'auctions/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.auctions = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('auctionController', function($scope, $http, $routeParams) 
{
    var id = $routeParams.id;
    $http({
        url: url + 'auctions/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.auction = success.data;
    }, function(error) {
        console.error(error);
    });
});