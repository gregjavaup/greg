package pl.greg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.greg.entity.Profession;
import pl.greg.entity.Specie;
import pl.greg.repository.ProfessionRepository;
import pl.greg.repository.SpecieRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/profession")
public class ProfessionController {

    @Autowired
    private ProfessionRepository professionRepository;


    @RequestMapping("/add")
    public Profession add(@RequestParam(name = "name") String name) {
        Profession p = new Profession();
        p.setName(name);
        return professionRepository.save(p);
    }

    @RequestMapping("/show")
    public List<Profession> showAll(){
        return (List<Profession>) professionRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Profession showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return professionRepository.findOne(myId);
    }

}
