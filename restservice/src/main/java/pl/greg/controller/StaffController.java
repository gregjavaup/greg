package pl.greg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.greg.entity.Animal;
import pl.greg.entity.Profession;
import pl.greg.entity.Specie;
import pl.greg.entity.Staff;
import pl.greg.repository.ProfessionRepository;
import pl.greg.repository.StaffRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Staff> listAnimals() {
        return (List<Staff>) staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addStaff  (@RequestParam(name = "name") String name,
                            @RequestParam(name = "lastname") String lastname,
                            @RequestParam(name = "salary") Float salary,
                            @RequestParam(name = "profession") String profession) {
        long professionId = Long.valueOf(profession);
        Profession p = professionRepository.findOne(professionId);
        Staff s = new Staff();
        s.setName(name);
        s.setLastName(lastname);
        s.setSalary(salary);
        s.setProfession(p);
        return staffRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public Staff showStaffByID(@PathVariable("id") String id) {
        return staffRepository.findOne(Long.valueOf(id));
    }
}
