package pl.greg.repository;

import org.springframework.data.repository.CrudRepository;
import pl.greg.entity.Animal;

public interface AnimalRepository extends CrudRepository<Animal, Long> {
}
