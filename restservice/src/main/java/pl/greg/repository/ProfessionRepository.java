package pl.greg.repository;

import org.springframework.data.repository.CrudRepository;
import pl.greg.entity.Profession;

public interface ProfessionRepository extends CrudRepository<Profession, Long> {
}
