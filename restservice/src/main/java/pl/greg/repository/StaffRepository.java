package pl.greg.repository;

import org.springframework.data.repository.CrudRepository;
import pl.greg.entity.Staff;

public interface StaffRepository extends CrudRepository<Staff, Long> {
}
