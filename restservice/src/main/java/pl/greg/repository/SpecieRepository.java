package pl.greg.repository;

import org.springframework.data.repository.CrudRepository;
import pl.greg.entity.Specie;

public interface SpecieRepository extends CrudRepository<Specie, Long> {

}
