var app = angular.module('firstApp', []);

app.controller('firstController', ['$scope', '$filter', function($scope, $filter) {
    $scope.message = "Wiadomość z kontrolera #1";
    $scope.user = {
        name: "Paweł",
        lastname: $filter('limitTo')("Testowy", 3), 
        salary: 1234.89
    };
    $scope.action = function() {
        alert($scope.imie);
    }
}]);

app.controller('secondController', function() {
    this.info = "Wiadomość z kontrolera #2";
});

app.controller('thirdController', ['$scope', '$log', 'limitToFilter', function($scope, $log, limitToFilter) {
    $scope.message = limitToFilter("Wiadomość z kontrolera #3", 10);
    
    $scope.myList = [
        'pierwsza',
        'druga',
        'trzecia',
        'czwarta',
        'piata'
    ];
    
    $scope.showMessage = function() {
        alert($scope.message + ' ' + $scope.$parent.message);
    }
}]);


var resJ = "{ 'rates' : [ { 'name': 'USD' }, { 'name': 'EUR' } ] }"; // JSON
var res = {
    rates: [
        {
            name: 'USD'
        },
        {
            name: 'EUR'
        }
    ]
}


app.controller('fourthC', ['$scope', '$http', function($scope, $http) {
    $scope.showCurrencies = function() {
        $http({
            url: 'https://randomuser.me/api/',
//            url: 'http://api.fixer.io/latest',
            method: 'GET',
            dataType: 'json',
            params: {}
        }).then(function(res) {
            var name = res.data.results[0].name;
            console.log(name.first + ' ' + name.last)
            /*$scope.rates = "";
            var rates = res.data.rates;
            var keys = Object.keys(rates);
            var ul = document.createElement('ul');
            for(let key of keys) {
                var li = document.createElement('li');
                li.innerHTML = key + ':' + rates[key];
                ul.appendChild(li);
            }
            document.getElementById('result').appendChild(ul);*/
        }, function(err) {
            console.warn(err);
        });
        console.log('czesc jestem grzegorz');
    }
}]);