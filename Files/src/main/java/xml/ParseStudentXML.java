package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import static xml.UtilsXML. *;
public class ParseStudentXML {

	public static void printStudentNode(NodeList nList, int temp) {
		Node nNode = nList.item(temp);
		System.out.println("\nCurrent Element :" + nNode.getNodeName());
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) nNode;
			System.out.println("Student roll no : " + student.getAttribute("rollno"));
			System.out.println("First Name : " + getText(student, "firstname"));
			System.out.println("Last Name : " + getText(student, "lastname"));
			System.out.println("Nick Name : " + getText(student, "nickname"));
			System.out.println("Marks : " +  getText(student, "marks"));
		}
	}

	public static void printStudentsInfo(Document doc) {
		NodeList students = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < students.getLength(); i++) {
			printStudentNode(students, i);
		}
	}

	public static void main(String[] args) {
		try {
			Document doc =  parseFile( FILE_NAME);
			printStudentsInfo(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
