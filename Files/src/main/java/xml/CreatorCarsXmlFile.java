package xml;

import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreatorCarsXmlFile {
	
	Document doc;
	
	public static void main(String argv[]) {

		new CreatorCarsXmlFile().create();
	}

	public void create() throws TransformerFactoryConfigurationError {
		try {
			doc = UtilsXML.newDocument();
			createContent();
			UtilsXML.saveFile(doc, "cars.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private  void createContent() {
		Element rootElement = createCars();
		Element ssupercars = addSuperCars(rootElement);

		UtilsXML.setAtribute(doc, ssupercars, "company", "Ferrari");

		addCar( ssupercars, "formula one", "Ferrari 101");
		addCar( ssupercars, "sports", "Ferrari 202");
	}

	private  void addCar( Element supercar, String type, String name) {
		
		Element carname =  doc.createElement("carname");
		UtilsXML.setAtribute(doc, carname, "type", type);
		carname.appendChild( doc.createTextNode(name));
		supercar.appendChild(carname);
	}

	private  Element addSuperCars( Element rootElement) {
		Element supercars =  doc.createElement("supercars");
		rootElement.appendChild(supercars);
		return supercars;
	}

	private  Element createCars() {
		Element rootElement =  doc.createElement("cars");
		 doc.appendChild(rootElement);
		return rootElement;
	}
}
