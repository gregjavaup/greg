package students;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Student> students = new ArrayList<>();
		students.add(new Student (100, "Jan", "Kowalski"));
		students.add(new Student (101, "Andrzej", "Duda"));
		students.add(new Student (102, "Pawel", "Franciszek"));
		students.add(new Student (103, "Janusz", "Bieniek"));
		students.add(new Student (104, "Marek", "Szczepanski"));
		
		University u = new 	University(students);
		
		IFile textFile = new XMLFile();
		textFile.save(u.getStudentList());
	
		List<Student> studentsFromFile = textFile.load();
		University universityFromFile = new University(studentsFromFile);
	
		universityFromFile.showAll();
	
	}

}
