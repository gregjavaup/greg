package students;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonFile implements IFile {
	
	private ObjectMapper mapper = new ObjectMapper();
	private final static String FILE_NAME = "studenci.jason";

	@Override
	public void save(List<Student> studentList) {
		
			try {
				mapper.writeValue(new File(FILE_NAME), studentList);
			} catch (IOException e) {
				e.printStackTrace();
			}
		

	}

	@Override
	public List<Student> load() {
		try {
	
		 List<Student> students = mapper.readValue(new File(FILE_NAME),  
		 new TypeReference<List<Student>>() {});
		 
		 return students;
		} 
		catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
