package students;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Student implements Serializable {
	private int indexNum;
	private String name;
	private String surname;
	
	// JSON wymaga geterów i seterów oraz pustego konstruktora

	public Student(int indexNum, String name, String surname) {
		this.indexNum = indexNum;
		this.name = name;
		this.surname = surname;
	}
	
	public void setIndexNum(int indexNum) {
		this.indexNum = indexNum;
	}


	public void setName(String name) {
		this.name = name;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public Student() {
		super();
	}

	

	public int getIndexNum() {
		return this.indexNum;
	}

	public String getName() {
		return name;
	}

	
	public String getSurname() {
		return surname;
	}
}
