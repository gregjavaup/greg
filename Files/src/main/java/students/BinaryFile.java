package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {
	private final static String FILE_NAME = "binary.txt";
	
	
	
	@Override
	public void save(List<Student> studentList) {
		
		try (FileOutputStream fout = new FileOutputStream(FILE_NAME);
		BufferedOutputStream buf = new BufferedOutputStream(fout);
		DataOutputStream out = new DataOutputStream(buf);) {
		
		for(Student s : studentList) {
			out.writeInt(s.getIndexNum());
			out.writeUTF(s.getName());
			out.writeUTF(s.getSurname());
		}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public List<Student> load() {
		List<Student> students = new ArrayList<>();
		try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))) {
			
			while(in.available() > 0) {
				int indexNum = in.readInt();
				String name =  in.readUTF();
				String surname =  in.readUTF();
				Student student = new Student(indexNum, name, surname);
				students.add(student);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
		
		return students;
	}

}
