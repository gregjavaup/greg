package students;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import students.Student;

public class University {

	private Map<Integer, Student> students = new HashMap<>();

	public University() {
	}

	public University(List<Student> studentsList) {
		for (Student s : studentsList) {
			students.put(s.getIndexNum(), s);
		}
	}

	public List<Student> getStudentList() {
		List<Student> NaszaLista = new ArrayList<Student>(students.values());
		return NaszaLista;
	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNum(), student);
	}

	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNum() + " : " + s.getName() + " " + s.getSurname());
		}
	}
	
	
	
}
