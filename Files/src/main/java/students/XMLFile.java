package students;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.CreatorCarsXmlFile;
import xml.UtilsXML;

public class XMLFile implements IFile {
	


	@Override
	public void save(List<Student> studentList) {
		create(studentList);
	}

	public void create(List<Student> studentList) throws TransformerFactoryConfigurationError {
		try {
			Document doc = UtilsXML.newDocument();
			createContent(doc, studentList);
			UtilsXML.saveFile(doc, "students.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent(Document doc, List<Student> studentList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);
		
		for(Student student : studentList) {
			Element studentElement =  doc.createElement("student");
			//UtilsXML.setAtribute(doc, studentElement, "index", String.valueOf(student.getIndexNum()));
			studentElement.setAttribute("index", String.valueOf(student.getIndexNum()));
			//studentElement.appendChild( doc.createTextNode(student.getName()));
		
			
			Element studentName = doc.createElement("name");
			studentName.appendChild( doc.createTextNode(student.getName()));
			studentElement.appendChild(studentName);
			//students.appendChild(studentElement);
			
			Element studentSurname = doc.createElement("surname");
			studentSurname.appendChild(doc.createTextNode(student.getSurname()));
			studentElement.appendChild(studentSurname);
			
			students.appendChild(studentElement);
			
		}
		
	}

	@Override
	public List<Student> load() {
		List<Student> studentList = new ArrayList();
	
		
		try {
			Document doc = UtilsXML.parseFile("students.xml");
			NodeList students = doc.getElementsByTagName("student");
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Student s = parse(studentNode);
				studentList.add(s);
			}
			
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return studentList;
	}

	private Student parse(Node studentNode) {
		Element student = (Element) studentNode;
		System.out.println(student.getAttribute("index"));
		int index = Integer.parseInt(student.getAttribute("index"));
		String name = UtilsXML.getText(student, "name");
		String surname = UtilsXML.getText(student, "surname");

		return new Student(index, name, surname);
	}
	
	

}
