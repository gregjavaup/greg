package students;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializedFile implements IFile {
	
	private final static String FILE_NAME = "serialized.txt";

	@Override
	public void save(List<Student> studentList) {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
			out.writeObject(studentList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
	}

	@Override
	public List<Student> load() {
		List<Student> students = null;
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILE_NAME))) { 
			students = (List<Student>) in.readObject();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		
		return students;
	}

}
