package oop;

public class Osoba {
	public String imie;
	public int wiek;
	public Osoba() {
		
	}
	
	public Osoba(String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}
	
	public void przedstawSie() {
		System.out.println(imie + " (" + wiek + ")");

	}

}
