SET SQL_SAFE_UPDATES = 0;
CREATE DATABASE IF NOT EXISTS StateAndCapital;
USE StateAndCapital;
DROP TABLE city;
ALTER TABLE city ADD COLUMN is_capital BOOLEAN DEFAULT 0;



CREATE TABLE state (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
 population INT,
 capital_id INT,
FOREIGN KEY (capital_id) REFERENCES city(id)
 );
 

CREATE TABLE city (id INT AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR (20) ,
 citizens INT
 );
 
 INSERT INTO CITY VALUES
(id, "Zagrzeb", 500000),
(id, "Berlin", 600000),
(id, "Londyn", 5200000),
(id, "Paryż", 5100000),
(id, "Gdansk", 500000),
(id, "Pierdziszewo", 200000),
(id, "Dortmund", 500000),
(id, "Soczi", 100000),
(id, "Dover", 500000),
(id, "Nottinhgam", 500000);




INSERT INTO state VALUES
(id, "Chorwacja", 20000000,1),
(id, "Niemcy", 80000000,2),
(id,  "Anglia", 60000000,3),
(id,  "Francja", 80000000,4),
(id,  "Norwegia", 15000000,5);

SELECT * FROM state LEFT JOIN city ON state.capital_id = city.id;
SELECT c.name AS capitol FROM state s RIGHT JOIN city c ON c.id =  s.capital_id WHERE s.name IS NOT NULL;

SELECT s.name AS state, c.citizens/s.population * 100 citizens FROM state s RIGHT JOIN city c ON s.capital_id = c.id;

SELECT s.name AS state, c.citizens/s.population * 100 citizens FROM state s RIGHT JOIN city c ON s.capital_id = c.id WHERE c.citizens/s.population * 100 > 3 ;


SELECT city.is_capital AS isCity FROM city RIGHT JOIN city ON state.capital_id = city.id;

UPDATE city SET is_capital = 1 WHERE EXISTS (SELECT * FROM state WHERE state.capital_id = city.id);

DELETE FROM city WHERE is_capital = 0;


SELECT * FROM city;
 
 