CREATE DATABASE company;
DROP TABLE worker;

CREATE TABLE worker (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(32),
salary DECIMAL(8,2),
age INT
);

INSERT INTO worker (name, salary, age) VALUES ("Jan", 1500, 25), ("Jan", 1600, 25), ("Jan", 1700, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 1800, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 1900, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 2000, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 2500, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 3500, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 4500, 35);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 2500, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 2500, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 2500, 25);
INSERT INTO worker (name, salary, age) VALUES ("Jan", 1350, 25);

SELECT * FROM worker;

SELECT max(salary), min(salary), avg(salary) FROM worker;

SELECT count(*), salary FROM worker GROUP BY salary;
SELECT avg(salary), age FROM worker GROUP BY age;

SELECT * FROM worker ORDER BY salary;
SELECT * FROM worker ORDER BY salary DESC;

SELECT * FROM worker ORDER BY salary LIMIT 1;
SELECT * FROM worker ORDER BY salary DESC LIMIT 1;
)) as 
SELECT * FROM worker WHERE salary = (SELECT mac(salary) FROM wrker);
SELECT abs(salary - (SELECT avg(salary) FROM worker)) as diff FROM worker ORDER BY diff;



