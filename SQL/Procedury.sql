
USE contacts;
DELIMITER //

CREATE PROCEDURE addContact()
BEGIN
	INSERT INTO contact (first_name, last_name) VALUE ("Adam","Nowak");
END;//
    DELIMITER ;

	CALL addContact();
    
    
SELECT count(*) FROM contact WHERE first_name = "Adam";

USE StateAndCapital;
DELIMITER //

CREATE PROCEDURE NewState()
BEGIN
	DECLARE id INT;
    
    INSERT INTO city (name.citizens) VALUE ("?", 100);
    SET id = last_insert_id();


	INSERT INTO state (name, population, capital_id) VALUE (state_name, 1000, id);
END;//
    DELIMITER ;

	CALL NewState("Węgry");
    
    SELECT * FROM NewState;
