/*Utwórz bazę shop z tabelami product (id,name,price,category_id), category (id,name,description).*/


CREATE DATABASE IF NOT EXISTS Shop;
USE Shop;

CREATE TABLE product (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
 price DECIMAL (8,2),
 category_id INT,
 FOREIGN KEY (category_id) REFERENCES category(id)
 );
DROP TABLE product;

CREATE TABLE category (id INT AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR (20) ,
 description TEXT
 );
 
 INSERT INTO category VALUES
(id, "Warzywa", "Promocja 50%"),
(id, "Owoce", "1 gratis przy zakupie 3"),
(id, "Mięso", "Przeterminowane mięso rabat 10%");

INSERT INTO product VALUES
(id, "Ziemniak", 10, 1),
(id, "Marchew", 2, 1),
(id, "Groszek", 1, 1),
(id, "Ogórek", 3, 1),
(id, "Jabłko", 1, 2),
(id, "Gruszka", 1, 2),
(id, "Arbuz", 10, 2),
(id, "Wieprzowina", 15, 3),
(id, "Wołowina", 25, 3),
(id, "Cielęcina", 32, 3);

SELECT p.name FROM category p;

SELECT p.name FROM product p
LEFT JOIN category ON product.name = category.name;

SELECT product.name, category.name FROM product LEFT JOIN category ON product.category_id = category.id;

SELECT c.name, count(*) FROM product p LEFT JOIN category c ON p.category_id = c.id GROUP BY category_id;

SELECT c.name,  avg(price) FROM product p LEFT JOIN category c ON p.category_id = c.id GROUP BY category_id;


/*widoki*/
CREATE VIEW product_name AS SELECT name FROM product;
SELECT * FROM product_name;

SELECT FROM procuct p LEFT JOIN category c ON p.category_id = c.id;


/*widoki*/
CREATE VIEW product_full AS 
SELECT p.id, p.name AS product_name, price, c.name AS category_name FROM product p LEFT JOIN category c ON p.category_id = c.id;
SELECT * FROM product_full;

DROP VIEW product_full;





