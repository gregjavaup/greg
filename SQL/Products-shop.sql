USE shop;

CREATE TABLE customer (
    id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(30),
   last_name VARCHAR(30)
   );
   
CREATE TABLE `order` (
    id INT AUTO_INCREMENT PRIMARY KEY,
   date DATE,
   paid ENUM ('NEW', 'PAID', 'CANCELED', 'COMPLETION'),
   customer_id INT, FOREIGN KEY (customer_id) REFERENCES customer(id)
   );
   
CREATE TABLE other_products (
   order_id INT, 
   product_id INT,
   quantity INT,
   FOREIGN KEY (order_id) REFERENCES `order`(id),
   FOREIGN KEY (product_id) REFERENCES product(id),
   PRIMARY KEY (order_id, product_id)
   );
   
SELECT * FROM product RIGHT JOIN category ON product.category_id = category_id;
SELECT * FROM product;
SELECT * FROM category;

DROP TABLE customer;

INSERT INTO customer VALUES 
(id, "Michał", "Pleszak"),
(id, "Adam", "Nowak");


INSERT INTO `order` VALUES 
(id, "2017-03-07", 'NEW', 1 ),
(id, "2017-03-07", 'NEW', 1 );

SELECT * FROM product;

INSERT INTO other_products VALUES 
	(1,2,3),
	(1,3,2),
	(1,4,3),
	(1,5,4),
	(1,6,10);


SELECT * FROM other_products;

SELECT name, quantity FROM other_products LEFT JOIN product ON other_products.product_id = product_id WHERE other_products.order_id = 1;

SELECT SUM(op.quantity) AS products_quantity, o.`date`, concat(c.first_name, " ", c.last_name) AS customer_name, o.paid, SUM(p.price*op.quantity) AS amount
FROM other_products op 
LEFT JOIN
product p  
ON p.id = op.product_id
LEFT JOIN
`order` o
ON op.order_id = o.id
LEFT JOIN
customer c
ON c.id = o.customer_id
GROUP BY o.id;






