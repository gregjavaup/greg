SET SQL_SAFE_UPDATES = 0;

CREATE DATABASE personal_data;
	create table personal_data.user (
	name varchar (20),
    pesel char(20)
    );
    
    create table person (
    first_name varchar(32),
    last_name varchar(32),
    age tinyint unsigned
    )
    
    select * from person;
    
    insert into person (first_name, last_name, age) values ("Jan", "Kowalski", 20);
    
    insert into person values ("Jan", "Kowalski", 20);
    
    select * from person;
    insert into person (first_name) values ("Dominik");
    
    select * from person WHERE age = 25;
    select * from person WHERE age IS NULL;
    select * from person limit 2;
    select (age) from person;
    
    
    
     SELECT * FROM person WHERE first_name like "Dom%";
     SELECT * FROM person WHERE age BETWEEN 20 and 30;
     
     SELECT first_name as 'Imie' FROM person;
     select avg(avg)  from person;
     
     SELECT count(*), age FROM person GROUP BY age;
     
     UPDATE contact SET e_mail = "Brak" WHERE e_mail IS NULL;
    