CREATE DATABASE contacts;

CREATE TABLE contact (
	id int AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    number VARCHAR(12),
    e_mail VARCHAR(32)
);

INSERT INTO contact (first_name, last_name, number) VALUES
("Jan", "Kowalski", "000-102-103"),
("Adam", "Kowalski", "672-555-555"),
("Marek", "Kowalski", "000-102-100"),
("Paweł", "Marcinkiewicz", "672-666-666"),
("Donatan", "Marcinkiewicz", "000-102-103");

SELECT * FROM contact;

 SELECT first_name,number FROM contact;
 
  SELECT concat(first_name," ",last_name) as names, e_mail FROM contact;
  SELECT concat(first_name," ",last_name) as names, e_mail FROM contact WHERE id = 3;
  
  SELECT count(*) from contact;

  SELECT * FROM contact WHERE last_name = "Marcinkiewicz";  

  SELECT * FROM contact WHERE number like "672%";  
  
    SELECT * FROM contact WHERE number like "%0";  

  DELETE FROM  contact;
  
  