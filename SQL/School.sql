
SET SQL_SAFE_UPDATES = 0;
CREATE DATABASE IF NOT EXISTS School;
USE School;
DROP TABLE city;

ALTER TABLE lesson ALTER COLUMN teacher_id BOOLEAN DEFAULT 0;


CREATE TABLE `subject` (
id INT AUTO_INCREMENT PRIMARY KEY,
subject_name VARCHAR (30)
   );
   

CREATE TABLE teacher (
id INT AUTO_INCREMENT PRIMARY KEY,
teacher_name VARCHAR (30),
teacher_surname VARCHAR(30)
   );

CREATE TABLE lesson_number (
id INT AUTO_INCREMENT PRIMARY KEY,
day_of_week ENUM ('MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY'),
start_time TIME,
end_time TIME
   );
   
   
 CREATE TABLE lesson (
PRIMARY KEY (lesson_number_id,teacher_id),
subject_id INT,
lesson_number_id INT,
teacher_id INT NOT NULL,
FOREIGN KEY(subject_id) REFERENCES `subject`(id),
FOREIGN KEY(lesson_number_id) REFERENCES lesson_number(id),
FOREIGN KEY(teacher_id) REFERENCES teacher(id)
);  



INSERT INTO teacher VALUES 
(id, "Katarzyna", "Zapała"),
(id, "Halina", "Wydra");

INSERT INTO lesson_number VALUES 
(id, '08:00', '08:45' ),
(id, '09:00' , '09:45'),
(id, '10:00' , '10:45'),
(id, '11:00' , '11:45'),
(id, '12:00' , '12:45');

INSERT INTO `subject` VALUES 
(id, "Matematyka"),
(id, "Fizyka"),
(id, "Biologia");


DROP TABLE lesson;

INSERT INTO lesson (subject_id, lesson_number_id, teacher_id) VALUES 
(1, 1, 1),
(2, 1, 2),
(3,2,1),
(1,2,2),
(3,3,2);

DROP VIEW lesson_full;

CREATE VIEW lesson_full AS
SELECT s.subject_name AS subcject, concat(t.teacher_name, " ", t.teacher_surname) AS teacher, ls.start_time AS start_time, ls.end_time AS end_time
FROM lesson l 
LEFT JOIN subject s ON l.subject_id = s.id
LEFT JOIN teacher t ON l.teacher_id = t.id
LEFT JOIN lesson_number ls ON l.lesson_number_id = ls.id;

SELECT s.subject_name AS subcject, concat(t.teacher_name, " ", t.teacher_surname) AS teacher, ls.start_time AS start_time, ls.end_time AS end_time
FROM lesson l 
LEFT JOIN subject s ON l.subject_id = s.id
LEFT JOIN teacher t ON l.teacher_id = t.id
LEFT JOIN lesson_number ls ON l.lesson_number_id = ls.id
WHERE l.subject_id = 1;

SELECT ls.start_time AS start_time, ls.end_time AS end_time, s.subject_name AS subcject, concat(t.teacher_name, " ", t.teacher_surname) AS teacher
FROM lesson l 
LEFT JOIN subject s ON l.subject_id = s.id
LEFT JOIN teacher t ON l.teacher_id = t.id
LEFT JOIN lesson_number ls ON l.lesson_number_id = ls.id
WHERE l.lesson_number_id BETWEEN 3 AND 5;


SELECT COUNT(*) AS `count`, subject.subject_name FROM lesson LEFT JOIN teacher ON lesson.teacher_id = teacher.id
LEFT JOIN `subject` ON lesson.subject_id = `subject`.id 
LEFT JOIN lesson_number ON lesson.lesson_number_id = lesson_number.id GROUP BY subject.subject_name;






   
   
   
   
