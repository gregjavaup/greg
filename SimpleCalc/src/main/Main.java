package main;

import calc.Matrix;
import calc.SimpleCalc;

public class Main {

	public static void main(String[] args) {
		SimpleCalc sc = new SimpleCalc();
		Matrix mx = new Matrix();
		int[][] expected = {{1,2,3}, {4,5,6}, {7,8,9}};
		mx.identityMatrix(expected);
		
		System.out.println(sc.add(Integer.MAX_VALUE, Integer.MIN_VALUE));
	}

}