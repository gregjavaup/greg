package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import calc.Matrix;

public class MatrixIdentityMatrixTest {
	private Matrix m;

	@Before
	public void makeInstance() {
		m = new Matrix();
	}
	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected2x2() {
		int[][] expected = {{1,0}, {0,1}};
		int[][] w = new int[2][2];
		assertArrayEquals(expected, m.identityMatrix(w));
	}
	
	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected3x3() {
		int[][] expected = {{1,0,0}, {0,1,0}, {0,0,1}};
		int[][] w = new int[3][3];
		assertArrayEquals(expected, m.identityMatrix(w));
	}
	
	@Test
	public void whenNotEmptyArrayGivenIdentityMatrixExpected3x3() {
		int[][] expected = {{1,2,3}, {4,5,6}, {7,8,9}};
		int[][] w = new int[3][3];
		m.identityMatrix(expected);
		m.identityMatrix(w);
		for(int i = 0; i < expected.length; i++) {
			assertTrue(expected[i][i] == w[i][i]);
		}
		
	}
	
	@Test
	public void whenEmptyArrayGivenChceckDimentionsOfArray() {
		int[][] expected = {{1,0,0}, {0,0}, {0,0,1}};
		int[][] actual = new int[3][3];
		actual = m.identityMatrix(actual);
		
		assertTrue(actual.length == expected.length);
		
		for(int i = 0; i < actual.length; i++) {
			assertTrue(actual[i].length == expected[i].length);
		}
		
	}
	
	@Test
	public void whenEmptyArrayGivenChceckIfArrayIsSquareLines() {
	int[][] expected = {{1,0,0}, {0,1, 0}, {0,0,1}};
	int[][] actual = new int[2][3];
	actual = m.identityMatrix(actual);
	
	assertTrue(actual.length == expected.length);
	
	
	}
	
	@Test
	public void whenEmptyArrayGivenChceckIfArrayIsSquareColumnes() {
	int[][] expected = {{1,0,0}, {0,1, 0}, {0,0,1}};
	int[][] actual = new int[3][2];
	actual = m.identityMatrix(actual);
	assertTrue(actual.length == expected.length);
	
		for(int i = 0; i < actual.length; i++) {
			assertTrue(actual[i].length == expected[i].length);
		}
	
	}
	
	@Test
	public void whenAllEmptyArrayGivenChceck() {
		ArrayIndexOutOfBoundsException ae = null;
		int[][] actual = new int[0][0];
		int[][] expected = {{1,0}, {1, 0}};
		try {
			actual = m.identityMatrix(actual);
		}catch (ArrayIndexOutOfBoundsException e) {
			ae = e;
		}
		assertNotNull("out of bound!!!", ae);
		
		
	}

}
