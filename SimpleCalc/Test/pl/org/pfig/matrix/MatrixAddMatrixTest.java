package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import calc.Matrix;

public class MatrixAddMatrixTest {
	
	private Matrix m;

	@Before
	public void makeInstance() {
		m = new Matrix();
	}

	@Test
	public void test() {
		IllegalArgumentException iae = null;
		int[][] x = new int[2][2];
		int[][] s = new int[2][2];
		try {
			m.addMatrix(x, s);
		} catch(IllegalArgumentException e) {
			iae = e;
		}
		
		assertNull("brak wyjatku", iae);
	}

}
