package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import calc.Matrix;

public class MatrixIsEqualDimensionTest {

	private Matrix m;

	@Before
	public void makeInstance() {
		m = new Matrix();
	}
	
	@Test
	public void tryTwoEqualArrays2x2() {
		int[][] x = new int[2][2];
		int[][] s = new int[2][2];
		assertTrue(m.isEqualDimension(x,s));
		
	}
	
	@Test
	public void tryTwoEqual1DArrays() {
		int[][] x = new int[2][0];
		int[][] s = new int[2][0];
		assertTrue(m.isEqualDimension(x,s));
	}
	@Test
	public void tryTwoNotEqualArrays() {
		int[][] x = new int[2][2];
		int[][] s = new int[2][8];
		assertFalse(m.isEqualDimension(x,s));
	}
	
	@Test
	public void tryWithOneEmpty2DArray() {
		int[][] x = new int[2][2];
		int[][] s = new int[0][0];
		assertFalse(m.isEqualDimension(x,s));
	}
	
	/*@Test
	public void tryWithMaxInt() {
		int[][] x = new int[Integer.MAX_VALUE /8][Integer.MAX_VALUE /8];
		int[][] s = new int[Integer.MAX_VALUE /8][Integer.MAX_VALUE /8];
		assertTrue(m.isEqualDimension(x,s));
	}
	*/
	/*@Test
	public void tryTwoEqualArraysWithNegativeArrays() {
		int[][] x = new int[-5][-5];
		int[][] s = new int[-5][-5];
		assertArrayEquals(x, s);
	}
*/
}
