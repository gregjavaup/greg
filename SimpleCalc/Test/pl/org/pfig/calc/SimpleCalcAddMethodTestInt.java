package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import calc.SimpleCalc;

public class SimpleCalcAddMethodTestInt {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff() {
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		
			int a = 3, b = 6;
			 sc = new SimpleCalc();
			//sc.add(a, b)
			assertEquals(9, sc.add(a, b));
	}
	
	@Test(expected = Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		sc.exThrow();
	}
	
	@Test
	public void whenOneIsNegativeAndOtherIsPositiveAndBiggerReturnsPositive() {
		
		int a = -2, b = 5;
		 sc = new SimpleCalc();
		
		assertEquals(3, sc.add(a, b));
	}
	
	@Test
	public void whenOneIsPositiveAndOtherIsNegativeAndBiggerReturnsNegative() {
		
		int a = 2, b = -5;
		 sc = new SimpleCalc();
		
		assertEquals(-3, sc.add(a, b));
	}

	@Test
	public void whenBoathIsNegatveReturnsNegative() {
		
		int a = -22, b = -55;
		 sc = new SimpleCalc();
		
		assertEquals(-77, sc.add(a, b));
	}
	
	@Test
	public void whenTwoNearMaxIntAreAdded() {
		int a = Integer.MAX_VALUE, b = Integer.MAX_VALUE;
		
		 sc = new SimpleCalc();
		
		//assertEquals(Integer.MAX_VALUE * 2, sc.add(a, b));
		assertFalse("Out of range", sc.add(a, b) > 0);
	}
	
	@Test
	public void whenMaxAndMinIntAreAdded() {
		int a = Integer.MAX_VALUE, b = Integer.MIN_VALUE;
		 sc = new SimpleCalc();
		
		//assertEquals(Integer.MAX_VALUE * 2, sc.add(a, b));
		assertFalse("Out of range", sc.add(a, b) >= 0);
	}
}
