package animals;

public class Land extends Animal {
	public Land(String name, int legs, boolean isMamle) {
		super(name, legs, isMamle);
	}
	
	@Override
	public void interestingInfoAbout() {
		System.out.println("They can live amongs us");
	}
	

}
