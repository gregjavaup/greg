package animals;

public abstract class Animal {
	private String name;
	private int legs;
	private String color;
	boolean isMamal;
	
	public Animal(String name, int legs, boolean isMamal) {
		this.name = name;
		this.legs = legs;
		this.isMamal = isMamal;
	}
	
	public String getName() {
		return name;
	}
	
	public int getLegs() {
		return legs;
	}
	public String setColor(String color) {
		 this.color = color;
		 return this.color;
	}
	
	public String getColor() {
		return color;
	}
	
	public boolean isMammal() {
		return isMamal;
	}
	public abstract void interestingInfoAbout();

}
