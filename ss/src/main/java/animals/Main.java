package animals;

public class Main {

	public static void main(String[] args) {
		
		Animal twitter = new Bird("Twitter", 2);
		Animal kaczka = new Bird("Kaczka", 2);
		
		if(!twitter.isMammal()) {
			System.out.println("is not a mamal");
		}
		twitter.interestingInfoAbout();
		kaczka.interestingInfoAbout();
		
		kaczka = new Land("Niedziedz", 4, true);
		System.out.println(kaczka.getName());
	}

}
