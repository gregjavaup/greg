package animals;

public class Fisch extends Animal {
	
	public Fisch(String name) {
		super(name, 0, false);
	}

	@Override
	public void interestingInfoAbout() {
		System.out.println("Fisch swim in the sea");
		
	}
}
