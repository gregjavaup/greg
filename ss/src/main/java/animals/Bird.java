package animals;

public class Bird extends Animal {
	
	public Bird(String name, int legs) {
		super(name, legs, false);
	}
	
	@Override 
	public void interestingInfoAbout() {
		System.out.println("Bird often files");
	}

}
