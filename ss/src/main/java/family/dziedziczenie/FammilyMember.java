package family.dziedziczenie;

public abstract class FammilyMember {
	private String name;
	
	public FammilyMember(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public abstract void introduce();
}
