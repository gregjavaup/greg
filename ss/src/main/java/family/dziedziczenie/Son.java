package family.dziedziczenie;

public class Son extends FammilyMember {
	

	
	public Son(String name) {
		super(name);
	}
	

	public  void introduce( ) {
		System.out.println("I’m a son. My name is " + this.getName());
	}

	
}
