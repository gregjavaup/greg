package Math;

public class Circle implements Figures {

	private double r;

	public Circle(double r) {
		this.r = r;
	}

	@Override
	public double countArea() {

		return (r * r) * Math.PI;
	}

	@Override
	public double countCircumference() {

		return 2 * Math.PI * r;
	}

}
