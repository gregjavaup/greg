package Math;

public class Square implements Figures {
	private double a;
	
	
	public Square(double a) {
		this.a = a;
	}
	
	@Override
	public double countArea() {
		
		return a*a;
	}

	@Override
	public double countCircumference() {
		return a*4;
	}

}
