package Math;

public interface Figures {
	
	double countArea();
	double countCircumference();

}
