package Math;

public class Triangle implements Figures {
	
		private double a;
		private double b;
		private double h;
	
	
	public Triangle (double a, double b, double h) {
		this.a = a;
		this.b = b;
		this.h = h;
	}

	@Override
	public double countArea() {
		
		return a/3 * h;
	}

	@Override
	public double countCircumference() {
		
		return a + 2*b;
	}

}
