import jdk.internal.org.objectweb.asm.tree.IntInsnNode;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class TabToList {

    public static void main(String[] args) {
        Integer tab[] = new Integer[] {1, 2, 3, 4, 5, 7, 8, 10};

        List<Integer> list = copyToList(tab);

        System.out.println(list.toString());
        copyToTab(list);
        toSquare(tab);

    }

    private static int[] toSquare(Integer[] tab) {
        int[] tabs = new int[tab.length];
        int iter = 0;
        for(int i : tab) {
            tabs[iter] = i*i;
            System.out.println(tabs[iter]);
        }

        return tabs;
    }

    private static <T> List<T> copyToList(T[] tab) {
        List<T> list = new ArrayList<T>();

        for(T i : tab) {
            list.add(i);
        }
        return list;
    }


    public static  int[] copyToTab(List<Integer> list) {
        int[] tab = new int[list.size()];
        int i = 0;

        for(int element : list) {
            tab[i] = element;
            System.out.println(element);
            i++;
        }

        return tab;
    }
}
