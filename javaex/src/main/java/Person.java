/**
 * Created by RENT on 2017-06-13.
 */

public class Person {
    private static String[] names = {"Karol ", "Maciek", "Sebek", "Daniel", "Marek", "Kondrad"};
    private static String[] surnames = {"Barzyła", "Chwala","Pompka", "Duduś", "Skiba", "Duda"};

    public static String randomPerson () {
        return RandomUtils.rand(names) + " " + RandomUtils.rand(surnames);
    }

    public static void main(String[] args) {
        System.out.println( randomPerson());
    }
}
