/**
 * Created by RENT on 2017-06-13.
 */
public class HowmannyLettersInString {

    private String str;

    public HowmannyLettersInString() {
    }

    public HowmannyLettersInString(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int howManyLetters(char chhr) {
        int count = 0;
        str = str.toLowerCase();
        System.out.println(str);
        for (int i = 0; i <= str.length() - 1; i++) {
            if(str.charAt(i) == chhr) {
                count ++;
            }

        }
    return count;
    }

    public static void main(String[] args) {

        HowmannyLettersInString hl = new HowmannyLettersInString("Blablacar");

        System.out.println(hl.howManyLetters('b'));

        hl.setStr("kunakurakolo");
        System.out.println(hl.howManyLetters('k'));



    }

}
