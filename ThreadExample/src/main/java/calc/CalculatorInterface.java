package calc;

/**
 * Created by RENT on 2017-06-19.
 */
public interface CalculatorInterface {
    public double doOperation(int a, int b);
}
