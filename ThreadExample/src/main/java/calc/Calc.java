package calc;

/**
 * Created by RENT on 2017-06-19.
 */
public class Calc {
    public int add(int a, int b) {
        return a+b;
    }
    public int subtract(int a, int b) {
        return a-b;
    }
    public int multiply(int a, int b) {
        return a*b;
    }
    public double devide(int a, int b) {
        return a/b;
    }
    public double oper(int a, int b, CalculatorInterface ci) {
        return ci.doOperation(a, b);
    }
}
