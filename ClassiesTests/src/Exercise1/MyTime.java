package Exercise1;

public class MyTime {
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	

	public MyTime() {
	}

	public MyTime(int hour, int minute, int second) {
		SetTime(hour, minute, second);
	}

	public void SetTime(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

	public void setHour(int hour) {
		if (hour >= 0 && hour <= 23) {
			this.hour = hour;
		} else {
			System.out.println("Z�y format, doba ma 24h");
		}
	}

	public void setMinute(int minute) {
		if (minute >= 0 && minute <= 59) {
			this.minute = minute;
		} else {
			System.out.println("Z�y format, godzina ma 60 minut");
		}
	}

	public void setSecond(int second) {
		if (second >= 0 && second <= 59) {
			this.second = second;
		} else {
			System.out.println("Z�y format, minuta ma 60 aekund");
		}
	}

	public String leadzero(int num) {
		if (num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}

	}

	@Override
	public String toString() {
		return " \"" + leadzero(hour) + ":" + leadzero(minute) + ":" + leadzero(second) + "\"";
	}

	public MyTime nextSecond() {
		if (second < 59)
			return new MyTime(hour, minute, second + 1);
		else if (minute < 59)
			return new MyTime(hour, minute + 1, second - 59);
		else
			return new MyTime(hour + 1, minute - 59, second - 59);

	}

	public MyTime nextMinute() {
		if (minute < 59) {
			return new MyTime(hour, minute + 1, second);
		} else if (hour < 23)
			return new MyTime(hour + 1, minute - 59, second);
		else
			return new MyTime(hour - 23, minute - 59, second);


	}

	public MyTime nextHour() {
		if (hour < 23)
			return new MyTime(hour + 1, minute, second);
		else
			return new MyTime(hour - 23, minute, second - 59);
	}
	
	public MyTime prevSecond() {
		if (second > 00)
			return new MyTime(hour, minute, second - 1);
		else if (minute > 00)
			return new MyTime(hour, minute - 1, second + 59);
		else
			return new MyTime(hour + 23, minute + 59, second + 59);

	}
	
	public MyTime prevMinute() {
		if (minute > 00) {
			return new MyTime(hour, minute - 1, second);
		} else if(hour > 00)
			return new MyTime(hour - 1, minute + 59, second);
		else
			return new MyTime (hour + 29, minute + 59, second);

	}
	
	public MyTime prevHour() {
		if (hour > 00)
			return new MyTime(hour - 1, minute, second);
		else
			return new MyTime(hour + 23, minute, second);
	}


}
