package grzesiek.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	Random generator = new Random();
	private final String path = "recources/";

	public User getRandomUser() {
		return null;
	}

	public User getRandomUser(UserSex sex) {
		return null;
	}

	public int countLines(String filename) {
		int lines = 0;
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
	
	public String getRandomLineFromFile(String filename) {


		String[] tab = new String[filename.length()];
		try (Scanner sc = new Scanner(new File(path + filename))) {
			
			int lines = 0;
			while (sc.hasNextLine()) {
				tab[lines++] = sc.nextLine();
				
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return tab[generator.nextInt(tab.length)];
		
	}
	

	
}
