package grzesiek.main;

public enum UserSex {
	SEX_MALE("Mężczyzna"),
	SEX_FEMALE("Kobieta"), 
	SEX_UNDEFINED("Nieokreślono");
	
	private String name;
	
	UserSex(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
