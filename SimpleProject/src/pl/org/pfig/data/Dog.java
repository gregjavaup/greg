package pl.org.pfig.data;

public class Dog implements AnimalInterface {
	private String name;
	
	

	@Override
	public String toString() {
		return "Dog: " + name;
	}

	public Dog(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

}
