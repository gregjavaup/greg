package pl.org.pfig.cars;

public class Viehicle extends Car {
	private String name;
	private int tires;
	private String engine;
	
	public Viehicle(String name, int tires, String engine) {
		this.engine = engine;
		this.name = name;
		this.tires = tires;
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public int getTires() {
		return this.tires;
	}
	

}
