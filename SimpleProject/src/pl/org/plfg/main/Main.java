package pl.org.plfg.main;

import pl.org.pfig.data.*;

public class Main {
	public static void main(String[] args) {

		Dog d1 = new Dog("Burek");
		Cat c1 = new Cat("Mruczek");
		llama l1 = new llama("Paragwaj");
		
		System.out.println("Piesek to: " + d1.getName());
		System.out.println("Kotek to: " + c1.getName());
		System.out.println("Lama to: " + l1.getName());
		System.out.println("Kotek robi: " + c1.getSound());
		
		AnimalInterface[] animals = new AnimalInterface[3];
		animals[0] = d1;
		animals[1] = c1;
		animals[2] = l1;
		for(AnimalInterface ai : animals) {
			System.out.println("Zwierze nazywa sie: " + ai.getName() + " (" + ai.getLegs() + ") ");
			if(ai instanceof Soundable) {
				Soundable simpleCat = (Soundable)ai;
				System.out.println("\t" + simpleCat.getSound());
			}
		}
		
			System.out.println(d1);
			
	}
	

}
