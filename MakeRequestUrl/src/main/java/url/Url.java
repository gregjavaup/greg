package url;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-07-14.
 */
public class Url {

    String host = "http://test.pl/";
    Map<String, String> params = new HashMap<String, String>();

    public static String put (String url, Map<String,String> values){
        String result = url.substring(0,url.length()-1);
        result +="?";
        for(Map.Entry entry : values.entrySet()) {
            result += entry.getKey() + "=" + entry.getValue() + "&";
        }
        result = result.substring(0,result.length()-1);
        return result;
    }


}
