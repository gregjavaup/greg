import connector.DatabaseConnector;
import model.City;
import resolver.FileConfigResolver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-07.
 */
public class Main {
    public static void main(String[] args) {

       Map<String, String> config = FileConfigResolver.resolve("db");
        System.out.println(config.get("dbhost"));
        for(Map.Entry e : config.entrySet()) {
            System.out.println(e.getKey() + " > = < " + e.getValue());
        }
        System.exit(0);
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = " INSERT INTO `city` VALUES (NULL, ?)";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1, "Gdansk");
            int countRows = ps.executeUpdate();
            System.out.println("Nadpisałem " + countRows + " wiersz(y).");
            ps.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
       try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "UPDATE `city` SET `city` = ? WHERE `city` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(2, "Sieradz");
            ps.setString(1, "Włocaławek");
            int countRows = ps.executeUpdate();
            System.out.println("Nadpisałem " + countRows + " wiersz(y).");
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "DELETE FROM `city` WHERE `id` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, 1);
            int countRows = ps.executeUpdate();
            System.out.println("Usunalem " + countRows + " rekord(y/ów)");
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();

            s.execute("INSERT INTO `city` VALUES(NULL, 'Wronki')");
            s.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

        // select
        List<City> listOfCities = new ArrayList<City>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                listOfCities.add(new City(rs.getInt("id"), rs.getString("city")));
            }

            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        for(City c : listOfCities) {
            System.out.println("> "  + c.getCity());
        }

    }
}
