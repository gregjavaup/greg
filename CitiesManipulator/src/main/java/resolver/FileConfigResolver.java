package resolver;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by RENT on 2017-07-07.
 */
public class FileConfigResolver {

    public static Map<String, String> resolve(String filename) {
        HashMap<String, String> ret = null;
        try {
            Scanner s = new Scanner(new File( "CitiesManipulator/src/main/resources/" + filename + ".config" ));
            ret = new HashMap<String,String>();
            while(s.hasNextLine()) {
                String line = s.nextLine();
                String[] splitted = line.split("=");
                ret.put(splitted[0].trim(), splitted[1].trim());
            }
            s.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}
