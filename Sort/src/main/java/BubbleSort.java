/**
 * Created by RENT on 2017-06-28.
 */
public class BubbleSort {

    public static void sort(int tab[]) {
        int buffor;
        for(int i = 0; i < tab.length; i++) {
            for (int j = tab.length -1; j > i ; j--) {
                if (!(tab[j - 1] < tab[j])) {
                    buffor = tab[j - 1];
                    tab[j - 1] = tab[j];
                    tab[j] = buffor;
                }
            }
        }
    }

}
