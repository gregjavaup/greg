package pl.org.pfig.main;

public class HTMLexercise {
	private String str = "";

	public HTMLexercise(String str) {
		this.str = str;
	}

	public HTMLexercise strong() {
		this.str = "<strong>" + this.str + "</strong>";
		return this;
	}

	public HTMLexercise p() {
		this.str = "<p>" + this.str + "</p>";
		return this;
	}

	public HTMLexercise print() {
		System.out.println(this.str);
		return this;
	}

}
