
package pl.org.pfig.main;

import java.util.Random;

import exercise.*;

public class Main {

	public static void main(String[] args) {
		StringUtil su = new StringUtil("przyk�adowy tekst");
		su.print().prepend("___").print().letterSpacing().print().reverse().print().getAlphabet().print()
		.getFirstLetter().print().prepend("ala ma kota").print().limit(6).print().insertAt("hehehe " , 3).print()
		.resetText().print();
		
		System.out.println("");
		
		StringUtil sus = new StringUtil("przyk�adowy tekst");
		sus.print().createSentence().print();
		
		System.out.println("");
		
		//String myStr = Przyklady();
//============================================================================================================
		
		/*ex1(myStr);
		System.out.println("!!");
		for(String s : checkWords(3, "Kiedys sie wybiore do lasu"))
			if(s != null) System.out.println(s);*/
		//Random();
		
		//HTMLexercise he = new HTMLexercise("To jest m�j tekst");
		//he.print().strong().print().p().print();
	}

	public static void Random() {
		Random r = new Random();
		// jezeli nie przekazujemy parametru, moze zostac zwrocona liczba ujemna
		System.out.println(r.nextInt()); 
		// jezeli przekazujemy argument, zostanie zwrocona liczba z zakresu <0, liczba)
		System.out.println(r.nextInt(15)); // <0, 15)
		
		// generowanie losowej liczby z zakesu
		// <10, 25) 25 - 10 = 15
		int a = 10;
		int b = 25;
		
		System.out.println(a + r.nextInt(b - a));
		
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c);
	}

	public static String Przyklady() {
		String myStr = "  przykladowy ciag  ";

		if (myStr.equals("  przykladowy ciag  "))
			System.out.println("Ci�gi s� takie same");

		if (myStr.equalsIgnoreCase("  PRZYKLADOWY ciaG  "))
			System.out.println("Ci�gi s� takie same, nie zwazajac na wielkosc liter");

		System.out.println("dlugosc myStr to: " + myStr.length());

		System.out.println(myStr.substring(14));
		String otherStr = "GRZEGORZ";

		System.out.println(otherStr.substring(1) + "!!!");
		System.out.println(otherStr.substring(1));
		System.out.println(otherStr.substring(1, otherStr.length() - 1));
		System.out.println(myStr);
		System.out.println(myStr.trim());
		System.out.println(myStr.charAt(2) + "" + myStr.charAt(3) + "!!!");

		String alphabet = "";
		for (char c = 97; c <= 122; c++) {
			alphabet += c;
			alphabet += " ";
		}

		System.out.println(alphabet);
		System.out.println(myStr.replace("ciag", "lancuch"));
		System.out.println(myStr.concat(otherStr));

		if (myStr.contains("klad")) {
			System.out.println("Slowo klad zawiea sie w: " + myStr);
		}

		if (alphabet.startsWith("a")) {
			System.out.println("alfabet zaczyna sie od A");
		}

		if (alphabet.endsWith("z ")) {
			System.out.println("alphabet konczy sie na Z");
		}

		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));

		String simpleString = "pawel poszedl do lasu";
		String[] arrOfStr = simpleString.split(" ");
		for (String s : arrOfStr) {
			System.out.println("\t" + s);
		}
		System.out.println(myStr.toUpperCase());
		System.out.println(myStr.toLowerCase());
		return myStr;
	}

	public static String[] checkWords(int how, String str) {
		String[] arrOfStr = str.split(" ");
		String[] arrOfStrRet = new String[arrOfStr.length];
		int i = 0;
		for(String s : arrOfStr) {
			if(s.length() > how) {
				arrOfStrRet[i] = s;
				i++;
			}
		}
		
		return arrOfStrRet;
		
	}

	public static void ex1(String myStr) {
		String mStr = "kaktus";
		char toReplace = mStr.charAt(0);
		mStr = mStr.substring(1);
		mStr = toReplace + mStr.replace(toReplace, '_');
		System.out.println(mStr);
	}

}
