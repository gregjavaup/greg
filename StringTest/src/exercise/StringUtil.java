package exercise;

public class StringUtil {
	private String str;

	public StringUtil(String str) {
		this.str = str;
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil letterSpacing() {
		String[] arr = str.split("");
		this.str = "";
		for (String s : arr) {
			this.str += s + " ";
		}
		str.substring(0, str.length());
		return this;
	}

	public StringUtil reverse() {
		String[] arr = str.split("");
		this.str = "";
		for (int lenght = arr.length - 1; lenght >= 0; lenght--) {
			this.str += arr[lenght];
		}
		return this;
	}

	public StringUtil getAlphabet() {
		this.str = "";
		for (char c = 97; c <= 122; c++) {
			this.str += c;
		}

		return this;
	}

	public StringUtil getFirstLetter() {
		this.str = str.substring(0, 1);
		return this;
	}

	public StringUtil limit(int n) {
		this.str = str.substring(0, n);
		return this;
	}

	public StringUtil insertAt(String string, int n) {
		// this.str = str.substring(0, n) + string + str.substring(n);
		String[] aar = str.split("");
		this.str = "";
		aar[n] += string;
		for (String s : aar) {
			this.str += s;
		}
		return this;
	}

	public StringUtil resetText() {
		this.str = "";
		return this;
	}

	public StringUtil swapLetters() {

		this.str = str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0);
		return this;
	}

	public StringUtil createSentence() {
		this.str = str.trim();
		String[] arr = str.split("");
		this.str = "";
		for (int i = 0; i<arr.length; i++) {
			this.str += arr[i];
				if(i == 0) {
					this.str = str.toUpperCase();
				}
			
			}
		if(!str.endsWith(".")) {
			this.str += ".";
		}
		
		return this;
		}

}


