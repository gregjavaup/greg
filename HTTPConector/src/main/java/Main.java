;import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by pablojev on 14.07.2017.
 */
public class Main {
    public static void main(String[] args) throws IOException {

        // GET

        URL url = new URL("https://www.facebook.com/loter.tanie.przeloty/");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String userAgent = "Pawel/1.0";

        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", userAgent);

        int responseCode = connection.getResponseCode();
        String result = "";
        if(responseCode == 200) {
            InputStream res = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(res);
            Scanner sc = new Scanner(reader);

            while(sc.hasNextLine()) {
                result += sc.nextLine();
                result += "\n";
            }
            sc.close();

        }
        System.out.println(result);


        // POST

        URL postURL = new URL("http://palo.ferajna.org/sda/wojciu/json.php");
        HttpURLConnection conPOST = (HttpURLConnection) postURL.openConnection();
        String postUA = "Pawel/2.0";

        conPOST.setRequestMethod("POST");
        conPOST.setRequestProperty("User-Agent", postUA);

        /*  tylko dla POST */
        String params = "login=admin";
        conPOST.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(conPOST.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();
        /* koniec tylko dla POST */

        Scanner sc = new Scanner(new InputStreamReader(conPOST.getInputStream()));
        String lines = "";
        while(sc.hasNextLine()) {
            lines += sc.nextLine();
        }
        sc.close();

        System.out.println(lines);
        JSONParser jp = new JSONParser();
                JSONArray ja = null;
                JSONObject jo = null;
                try {
                        Object o = jp.parse(lines);
                        if(o instanceof JSONArray) {
                                System.out.println("to jest array");
                                ja = (JSONArray) o;
                            } else if(o instanceof JSONObject) {
                                System.out.println("to jest obiekt");
                                jo = (JSONObject) o;
                            }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                        if(jo != null) {

                                System.out.println("status: " + jo.get("status"));
                    }
    }
}