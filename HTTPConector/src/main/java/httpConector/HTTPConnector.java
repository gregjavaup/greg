package httpConector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by RENT on 2017-07-14.
 */
public class HTTPConnector {
    private URL object;
    private HttpURLConnection connection;
    private String userAgent;


        public  String sendGET(String url) throws IOException {
            this.object = new URL(url);
            this.connection = (HttpURLConnection) object.openConnection();
            userAgent ="Greg/1.0";

            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", userAgent);

            int responseCode = connection.getResponseCode();
            String result = "";
            if(responseCode == 200){

                InputStream res = connection.getInputStream();
                InputStreamReader reader = new InputStreamReader(res);
                Scanner sc = new Scanner(reader);
                while(sc.hasNextLine()){
                    result += sc.nextLine();
                } sc.close();
                System.out.println(result);
            }
            return result;
        }


    public String sendPOST(String url) throws IOException{
        this.object = new URL(url);
        this.connection = (HttpURLConnection) object.openConnection();
        userAgent ="Greg/1.0";

        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", userAgent);

        /*  tylko dla POST */
        String params = "login=admin";
        connection.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();
        /* koniec tylko dla POST */

        Scanner sc = new Scanner(new InputStreamReader(connection.getInputStream()));
        String lines = "";
        while(sc.hasNextLine()) {
            lines += sc.nextLine();
        }
        sc.close();

        return lines;
    }

    }


