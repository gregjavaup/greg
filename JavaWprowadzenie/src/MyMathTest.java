import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void zwrocWieksza() {
		assert MyMath.zwrocWieksza(2, 4) == 4;
		assert MyMath.zwrocWieksza(2L, 4L) == 4L;
		assert MyMath.zwrocWieksza(2F, 4F) == 4F;
		assert MyMath.zwrocWieksza(2D, 4D) == 4D;

	}
	@Test
	public void zwrocMniejsza() {
		assert MyMath.zwrocMniejsza(2, 4) == 2;
		assert MyMath.zwrocMniejsza(2l, 4l) == 2l;
		assert MyMath.zwrocMniejsza(2f, 4f) == 2f;
		assert MyMath.zwrocMniejsza(2.0d, 4.0d) == 2d;
	}
	@Test
	public void abs() {
		assert MyMath.abs(-2) == 2;
		assert MyMath.abs(2) == 2;
		assert MyMath.abs(-2l) == 2l;
		assert MyMath.abs(-2f) == 2f;
		assert MyMath.abs(-2.0d) == 2;
		
	}
	@Test
	public void pow() {
		assert MyMath.pow(2, 2) == 4;
		assert MyMath.pow(2l, 2) == 4l;
		assert MyMath.pow(2f, 2) == 4.0f;
		assert MyMath.pow(2.5d, 2) == 6.25d;
	}

}
