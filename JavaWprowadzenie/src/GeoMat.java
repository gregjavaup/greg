
public class GeoMat {
	
	public static double squereArea (double a) {
		if(a>=0){return a*a;}
		
		else
			return 0;
	}
	
	public static double qubicArea (double a) {
		return 6*squereArea(a);
		
	}
	
	public static double cercilurArea(double r) {
		return (r * r) * Math.PI;
		
		}
	
	public static double cylinderVolume(double r, double h) {
		return cercilurArea(r) * h;
	}
	
	public static double coneVolume(double r, double h) {
		return cylinderVolume(r, h) / 3;
	}
	
	public static double qubicVolume (double a) {
		return squereArea(a) * a;
		
	}
	public static double sqrCone(double a, double h) {
		return (squereArea(a) * h) / 3;
	}
	
	}

