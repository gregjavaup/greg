

import org.junit.Test;

public class GeoMatTest {
	
	@Test
	public void sqrTest() {
		assert GeoMat.squereArea(2.0) == 4.0;
		assert GeoMat.squereArea(1.5) == 2.25;
		assert GeoMat.squereArea(0) == 0;
		assert GeoMat.squereArea(-2.0) == 0.0;
		assert GeoMat.squereArea(2.0) == 4.0;
		
	}
	
	@Test
	public void qubicTest() {
		assert GeoMat.qubicArea(2) == 24;

		
	}
	
	@Test
	public void cercularTest() {
		assert GeoMat.cercilurArea(2.0) > 12.56;
		assert GeoMat.cercilurArea(2.0) < 12.57;
	}
	
	@Test
	public void cylinderTest() {
		assert GeoMat.cylinderVolume(2, 4) > 50.26;
		assert GeoMat.cylinderVolume(2, 4) < 50.27;
	}
	
	@Test
	public void coneTest() {
		assert GeoMat.coneVolume(2, 5) > 20.94;
		assert GeoMat.coneVolume(2, 5) < 20.95;
	}
	@Test
	public void qubicVolumeTest() {
		assert GeoMat.qubicVolume(1) == 1;
		assert GeoMat.qubicVolume(2) == 8;
		assert GeoMat.qubicVolume(11.7) > 1601;
		assert GeoMat.qubicVolume(11.7) < 1602;
	}
	
	@Test
	public void sqrCone() {
		assert GeoMat.sqrCone(3, 3) == 3;
		assert GeoMat.sqrCone(3, 3) == 3;
		assert GeoMat.sqrCone(3, 3) == 3;
		assert GeoMat.sqrCone(3, 3) == 3;
	}
}
