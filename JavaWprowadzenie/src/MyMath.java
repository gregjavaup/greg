

public class MyMath {
	//======================================================
	public static int zwrocWieksza(int a, int b) {
		
		return (a < b) ? b : a;
	}
	public static long zwrocWieksza(long a, long b) {
		
		return (a < b) ? b : a;
	}
	public static float zwrocWieksza(float a, float b) {
		
		return (a < b) ? b : a;
	}
	public static double zwrocWieksza(double a, double b) {
		
		return (a < b) ? b : a;
	}
	//======================================================	
	
	public static int  zwrocMniejsza(int a, int b) {
		return (a > b) ? b : a;
	}

	public static long  zwrocMniejsza(long a, long b) {
		return (a > b) ? b : a;
	}
	
	public static float  zwrocMniejsza(float a, float b) {
		return (a > b) ? b : a;
	}
	
	public static double  zwrocMniejsza(double a, double b) {
		return (a > b) ? b : a;
	}
	//======================================================	
	
	public static int abs(int a) {
		return (int) Math.sqrt(a*a);
	}
	public static long abs(long a) {
		return (long) Math.sqrt(a*a);
	}
	public static float abs(float a) {
		return (float) Math.sqrt(a*a);
	}
	public static double abs(double a) {
		return Math.sqrt(a*a);
	}
	//======================================================	
	
	public static int pow(int a, int b) {
		int x = 1;
		for(int i = 0; i<b; i++) {
			x *= a;
		}
			return x;
	}
	
	public static long pow(long a, int b) {
		long x = 1;
		for(long i = 0; i<b; i++) {
			x *= a;
		}
			return x;
	}
	
	public static float pow(float a, int b) {
		float x = 1;
		for(int i = 0; i<b; i++) {
			x *= a;
		}
			return x;
	}
	
	public static double pow(double a, int b) {
		double x = 1;
		for(int i = 0; i<b; i++) {
			x *= a;
		}
			return x;
	}
	
}
