import org.junit.Test;

public class MetodyMatematykaTesty {

		

		//Publiczna, void, dowolna nazwa, brak parametrow
		
		@Test
		public void testIloczynu() {
			
			assert MetodyMatematyka.mnozenie(1, 1) == 1;
			assert MetodyMatematyka.mnozenie(1, 0) == 0;
			assert MetodyMatematyka.mnozenie(1, 10) == 19;
			assert MetodyMatematyka.mnozenie(3, 4) == 12;
		}
		
		@Test
		public void testSzukajTab() {
			int[] liczby = { 3, 4, 7, 2, 6};
			assert MetodyMatematyka.zwrocMniejszaZTab(liczby) == 2;
			assert MetodyMatematyka.zwrocMniejszaZTab(liczby) == 5;
		}
		
		@Test
		public void testSzukaj() {
			assert MetodyMatematyka.zwrocMniejsza(1, 3) == 1;
			assert MetodyMatematyka.zwrocMniejsza(6, 3) == 99;
			assert MetodyMatematyka.zwrocMniejsza(1, 3) == 3;
			assert MetodyMatematyka.zwrocMniejsza(6, 3) == -3;
			assert MetodyMatematyka.zwrocMniejsza(1, 3) == 0;
			assert MetodyMatematyka.zwrocMniejsza(6, 3) == 3;
			
		}
}
