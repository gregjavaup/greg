import java.util.Scanner;

public class Tablice {

	public static void main(String[] args) {

		Scanner sc = new Scanner (System.in);
		
		//zad1();
		//zad2(sc);
		zad3(sc);
		
	}

	private static void zad3(Scanner sc) {
		
		
	    String[] imion = {"adam", "maciek", "pawel", "adi", "seba", "kondziu"};
		
		for(String im : imion)
			System.out.println(im);
		
		System.out.println("");
		
		for(int i = 0; i < imion.length; i+= 2)
			System.out.println(imion[i]);
		
		System.out.println("");
		
		for(String im : imion)
		{
			char napis = im.charAt(0);
			if(napis == 'a')
				System.out.println(im);
				
		}
	}

	
	
	private static void zad2(Scanner sc) {
		System.out.println("Ile elementow ma miec tablica?: ");

		int x;
		x = sc.nextInt();
		int tab[] = new int[x];
		
		System.out.println("Podaj liczby w tablicy");
		
		for(int i = 0; i < x; i++)
		{
			tab[i] = sc.nextInt();
			System.out.println(tab[i]);
		}
		
		System.out.println("ok");
	}

	private static void zad1() {
		int[] tab = {1, 3, 5, 10};
	
		
		System.out.println(tab[0]);
		System.out.println(tab[1]);
		System.out.println(tab[2]);
		System.out.println(tab[3]);
		
		for(int x = 0; x <= tab.length - 1; x++)
			System.out.println(tab[x]);
		
		for(int x = tab.length - 1; x<=0; x--)
			System.out.println(tab[x]);
		
		for(int x : tab)
			System.out.println(x);
	}

}
