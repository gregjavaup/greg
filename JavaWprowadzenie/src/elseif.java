
public class elseif {

	public static void main(String[] args) {
		System.out.println("2>3");
		
		if(2 > 3)
			System.out.println(":)");
		else
			System.out.println(":(");
		
		System.out.println("4 < 5");
		
		if(4 < 5)
			System.out.println(":)");
		else
			System.out.println(":(");
		
		System.out.println("(2 - 2) == 0");
		
		if((2 - 2) == 0)
			System.out.println(":)");
		else
			System.out.println(":(");
		
		System.out.println("true");
		
		if(true)
			System.out.println(":)");
		else
			System.out.println(":(");

		System.out.println("9%2 == 0");
		
		if(9%2 == 0)
			System.out.println(":)");
		else
			System.out.println(":(");

		System.out.println("9%3 == 0");
		
		if(9%3 == 0)
			System.out.println(":)");
		else
			System.out.println(":(");
		

	}

}
