
public class MetodyMatematyka {

	public static void main(String[] args) {
		int a = 5;
		int b = 2;
		
		sumowanie(a, b);
	
		System.out.println("Wynik mnozenia: " + a + "*" + b +" = " + mnozenie(a, b));
		
		
		System.out.println("Mniejsza z liczb " + a + " i " + b + " = " + zwrocMniejsza(a, b));
		
		int c = 9;
		System.out.println("Mniejsza z liczb " + a + " i " + b + " i " + c + " = " + zwrocMniejsza(a, b, c));
		
		int tab[] = {3, 4, 6, 77, 8, 4, 1, 11};
		System.out.println("Mniejsza z tab{3, 4, 6, 77, 8, 4, 17, 11} " + " = " + zwrocMniejszaZTab(tab));
		
	 	
	}

	public static int zwrocMniejszaZTab(int[] tab) {
		
		if(tab.length == 0)
			return 0;
		int ret = zwrocMniejsza(tab[0], tab[1]);
		
		for(int i = 2; i<tab.length; i++)
			ret = zwrocMniejsza(ret, tab[i]);
		
		return ret;
		
	}

	public static int zwrocMniejsza(int a, int b, int c)
	{
		return zwrocMniejsza(zwrocMniejsza(a, b), c);
	}
	
	public static int zwrocMniejsza(int a, int b) {
	
		return (a > b) ? b : a;

	}

	public static int mnozenie(int a, int b)
	{
		return (a*b);
	}
	
	private static void sumowanie(int a, int b) {
		System.out.println("Wynik sumowania: " + a + "+" + b +" = " + (a + b));
	}

}
