package pl.grzesiek.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	Random generator = new Random();
	private final String path = "recources/";

	public User getRandomUser() {

		UserSex us = UserSex.SEX_MALE;
		if (new Random().nextInt(2) == 0) {
			us = UserSex.SEX_FEMALE;
		}

		return getRandomUser(us);
	}

	public User getRandomUser(UserSex sex) { // ############################################################################
		User currentUser = new User();
		currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if (sex.equals(UserSex.SEX_MALE)) {
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}

		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));

		randomAddressGenerator(currentUser);
		currentUser.setPhone(randomNumber(9));
		randomCCN(currentUser);

		String dt = getRandomBirthDate(); // dd.mm.yy
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}

		try {
			currentUser.setPESEL(generatePESEL(currentUser, sdf.parse(dt)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return currentUser;

	}// ####################################################################################################################

	private String generatePESEL(User currentUser, Date dt) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String[] splitted = sdf.format(currentUser.getBirthDate()).split("");
		int[] century = new int[4];
		int centuryFinal = 0;
		for (int s = 6, o = 0; s < 10; s++, o++) {
			century[o] = splitted[s].charAt(0);
			century[o] -= 48;
		}
		
		for(int i = 0; i < 4; i++) {
			centuryFinal *= 10;
			centuryFinal += century[i];
			
		}
		if(centuryFinal >= 1900 && centuryFinal < 1999) {
			String PESEL = "" + splitted[8] + splitted[9] + splitted[3] + splitted[4] + splitted[0] + splitted[1];
		} 

		PESEL += randomNumber(3);
		Random rand = new Random();
		int i = rand.nextInt(10);
		if (currentUser.getSex().toString() == "SEX_MALE") {
			if (i % 2 == 0) {
				i--;
			}
			PESEL += "" + i;
		} else {
			if (!(i % 2 == 0)) {
				i--;
			}
			PESEL += "" + i;
		}

		String[] arr = PESEL.split("");
		int[] arint = new int[10];
		for (int o = 0; o < 10; o++) {
			arint[o] = arr[o].charAt(0);
			arint[o] -= 48;
		}
		// 9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
		int helpNumb = arint[0] * 9 + arint[1] * 7 + arint[2] * 3 + arint[3] * 1 + arint[4] * 9 + arint[5] * 7
				+ arint[6] * 3 + arint[7] * 1 + arint[8] * 9 + arint[9] * 7;
		int lastNumb = helpNumb % 10;

		PESEL += "" + lastNumb;
		return PESEL;
	}

	private String getRandomBirthDate() {

		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
			daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year; // dd.MM.YYYY;

	}

	private String leadZero(int arg) {
		if (arg < 10)
			return "0" + arg;
		else
			return "" + arg;
	}

	public void randomCCN(User currentUser) {
		String[] CCN = randomNumber(16).split("");
		String[] CCNFinal = new String[] { "", "", "", "" };
		int o = 0;
		for (int i = 0; i < 4; i++) {
			CCNFinal[i] += CCN[o++];
			CCNFinal[i] += CCN[o++];
			CCNFinal[i] += CCN[o++];
			CCNFinal[i] += CCN[o++] + " ";
		}

		currentUser.setCCN((" " + CCNFinal[0] + CCNFinal[1] + CCNFinal[2] + CCNFinal[3]));
	}

	public String randomNumber(int numbersElements) {
		int phoneline = 0;
		String phoneNumber = "";
		Random rand = new Random();
		for (int i = 0; i < numbersElements; i++) {
			phoneline = rand.nextInt(10);
			phoneNumber += phoneline;
		}

		return phoneNumber;
	}

	public void randomAddressGenerator(User currentUser) {
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("Ul." + getRandomLineFromFile("street.txt"));
		adr.setNumber("" + (new Random().nextInt(100) + 1));
		currentUser.setAddress(adr);
	}

	public int countLines(String filename) {
		int lines = 0;
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}

	public String getRandomLineFromFile(String filename) {

		String[] tab = new String[countLines(filename)];
		try (Scanner sc = new Scanner(new File(path + filename))) {

			int lines = 0;
			while (sc.hasNextLine()) {
				tab[lines++] = sc.nextLine();

			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return tab[generator.nextInt(tab.length) - 1];

	}

}
