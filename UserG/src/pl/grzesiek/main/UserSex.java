package pl.grzesiek.main;

public enum UserSex {
	SEX_MALE, SEX_FEMALE, SEX_UNDEFINED;
}
