package defoult;

/**
 * Created by RENT on 2017-06-29.
 */
 class Bike implements Methods {
    private String brand;
    int seats, gears;
    BIKE_TYPE type;

    public Bike(String brand, int seats, int gears, BIKE_TYPE type) {
        super();
        this.brand = brand;
        this.seats = seats;
        this.gears = gears;
        this.type = type;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public int getSeats() {
        return seats;
    }
    public void setSeats(int seats) {
        this.seats = seats;
    }
    public int getGears() {
        return gears;
    }
    public void setGears(int gears) {
        this.gears = gears;
    }
    public BIKE_TYPE getType() {
        return type;
    }
    public void setType(BIKE_TYPE type) {
        this.type = type;
    }

    public void toStringg() {
        System.out.println(this.brand + " " + this.type);
    }
}
