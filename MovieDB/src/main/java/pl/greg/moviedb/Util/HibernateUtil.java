package pl.greg.moviedb.Util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.concurrent.locks.Condition;


/**
 * Created by RENT on 2017-07-13.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSesionFactory();
    private static  sesja = null;

    private static SessionFactory buildSesionFactory() {
        if(sesja == null){
            sesja = new Configuration().configure().buildSessionFactory();
            return sesja;
        }
        else {
            return sesja;
        }
    }

    private static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session openSession() {
        return getSessionFactory().openSession();
    }
}
