package pl.greg.moviedb;

import pl.greg.moviedb.dao.MovieDAO;
import pl.greg.moviedb.entity.Movie;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-13.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj nazwe filmu w formacie: \"Tytuł Rok CzasTrwania(w minutach) Opis\" ");
        String odczyt = sc.nextLine();
        String[] spl = odczyt.split(" ");

        MovieDAO mdao = new MovieDAO();
        Movie m = new Movie(spl[0]);
        m.setYear(Integer.parseInt(spl[1]));
        m.setDuration(Integer.parseInt(spl[2]));
        m.setDiscription(spl[3]);
        mdao.insert(m);

        for(Movie movie : mdao.get()) {
            System.out.println(movie.getId() + ". " + movie.getName() + ". " + movie.getYear() + " minut. " + movie.getDuration() + ". " + movie.getDiscription());
        }

    }
}
