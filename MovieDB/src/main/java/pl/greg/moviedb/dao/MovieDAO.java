package pl.greg.moviedb.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.greg.moviedb.Util.HibernateUtil;
import pl.greg.moviedb.entity.Movie;

import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public class MovieDAO implements AbstractDAO<Movie> {
    public void insert(Movie type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();
    }

    public boolean delete(Movie type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        session.close();
        return true;

    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
        return true;
    }

    public boolean update(Movie type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();
        return true;
    }

    public Movie get(int id) {
        Movie movies;
        Session session = HibernateUtil.openSession();
        movies = session.load(Movie.class, id);
        session.close();
        return movies;
    }

    public  List<Movie> get() {
        List<Movie> movies;
        Session session = HibernateUtil.openSession();
        movies = session.createQuery("from Movie").list();
        session.close();
        return movies;
    }
}
