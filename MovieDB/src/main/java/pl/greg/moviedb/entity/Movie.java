package pl.greg.moviedb.entity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="year")
    private int year;

    @Column(name="duration")
    private double duration;

    @Column(name="discription")
    private String discription;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Genere> genereList = new LinkedList<Genere>();

    public Movie() {
    }

    public Movie(String Name) {
        this.name = Name;
    }

    public Movie(int id,String name) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }
}
