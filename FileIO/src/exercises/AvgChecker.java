package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class AvgChecker extends LengthChecker {
	private String filename;
	private final String path = "Recources/";

	public AvgChecker(String filename) {
		this.filename = filename;
	}

	public void process() {
		File f = new File(path + filename);
		String currentLine;
		int clines = countLines(path + filename);

		
		double scoreAll = 0;
		double[] scoreTab = new double[clines];
		String[] names = new String[clines];
		int o = 0;
		int j = 0;
		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				if (!currentLine.equals("")) {
					String[] temp = currentLine.split("\t");
					double score = 0;
					int i = 1;
					for (; i < temp.length; i++) {
						score += Double.parseDouble(temp[i]);
						j++;
					}
					names[o] = temp[0];
					scoreTab[o] = score / (i - 1);
					scoreAll += score;
					o++;
				}
			}
			scoreAll = scoreAll / j;

			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			System.out.println(scoreAll);
			for (int b = 0; b < names.length; b++) {
				if (scoreTab[b] >= scoreAll) {
					pw.println(names[b] + "\t" + scoreTab[b]);
				}
			}

			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

}
