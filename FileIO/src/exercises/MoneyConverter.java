package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	private final String filename = "Recources/currency.txt";
	
	
	

	public double readCourse(String currency) {
		File f = new File(filename);

		try (Scanner sc = new Scanner(f);) {
			String currentLine;

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t");
				String[] values = temp[0].split(" ");
				if (values[1].equalsIgnoreCase(currency)) {
					return Double.parseDouble(temp[1].replace(",", ".")) 
							/ Integer.parseInt(values[0]);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
	public double convert(double money, String to) {
		
		return money/ this.readCourse(to);
	}
	
	public double convert(double money, String to, String from) {
		
		return (money * this.readCourse(from)) / this.readCourse(to);
	}
	
	
}
