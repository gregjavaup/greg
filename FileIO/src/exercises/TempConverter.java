package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	private final String filename = "Recources/tempC.txt";
	

	
	public double[] readTemp(String filename) { 
		double[] ret = new double[countLines(this.filename)];
		
		File f = new File(this.filename);
		try(Scanner sc = new Scanner(f)) {
			String curline;
			int i = 0;
			while(sc.hasNextLine()) {
				curline = sc.nextLine();
				ret[i++] = Double.parseDouble(curline.replace(",", "."));
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return ret;
	}

	private int countLines(String filename) {
		File f = new File (filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}
	
	public double toKelvin(double temp) {
		return temp + 273.15;
	}
	
	public double toFahrenheit(double temp) {
		return (temp * 1.8) + 32;
	}
	
	public void writeTemp(double[] temp) {
		File toK = new File("Recources/tempK.txt");
		File toF = new File("Recources/tempF.txt");
		
		try {
			FileOutputStream foK = new FileOutputStream(toK);
			FileOutputStream foF = new FileOutputStream(toF);
			
			PrintWriter pwK = new PrintWriter(foK);
			PrintWriter pwF = new PrintWriter(foF);
			
			for(double c: temp) {
				pwK.println(toKelvin(c));
				pwF.println(toFahrenheit(c));
			}
			pwK.close();
			pwF.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}
}