package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {
	private final String path = "Recources/";
	private String filename;
	private double[] curColumn;
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner() {
	}

	public Columner(String filename) {
		this.filename = filename;
	}
	
	private void readFile() {
		File f = new File(path + filename);
		try{
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				fileContent.add(sc.nextLine());
			}
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void readColumn(int column) {
		readFile();
		int maxColumn = fileContent.get(0).split("\t").length;
		if(column >= 1 && column <= maxColumn) {
			curColumn = new double [fileContent.size()];
			int i = 0;
			for(String line : fileContent) {
				String[] cols = line.split("\t");
				curColumn[i++] = Double.parseDouble(cols[column -1]);
			}
		}
	}

}
