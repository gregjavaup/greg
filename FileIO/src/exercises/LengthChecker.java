package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {


	public int countLines(String filename) {
		File f = new File (filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}
	
	private String[] readFile(String filename) {

		String[] ret = new String[countLines(filename)];
		
		File f = new File(filename);
		
		try(Scanner sc = new Scanner(f)) {
			int i = 0;
			while(sc.hasNextLine()) {
				ret[i++] = sc.nextLine();
			}
			
			
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	
	private boolean isProperLeght(String arg, int len) {
		
		return arg.length() > len;
		
		
	}
	private void writeFile(String[] fileContent, int len) {
		
		try {
			FileOutputStream fos = new FileOutputStream("Recources/words_" + len + ".txt");
			PrintWriter pw = new PrintWriter(fos);
			for(String s : fileContent) {
				
				if(isProperLeght(s, len))
					pw.println(s);
				
				
			}
			pw.close();
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void make(String fileInput, int len) {
		String[] str = readFile(fileInput);
		writeFile(str, len);
	}
}
