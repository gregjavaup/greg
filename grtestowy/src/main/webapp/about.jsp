<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp" />

<section class="mainContent">
    <p>
        234324 23 432 432Lorem ipsum Lorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsum
        Lorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsum
        Lorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsumLorem ipsum
    </p>
    <p>Imie: <c:out value="${imie}" /></p>
    <ul>
    <c:forEach items="${colors}" var="color">
        <li><c:out value="${color}" /></li>
    </c:forEach>
    </ul>
</section>

<c:import url="sidebar.jsp" />
<c:import url="footer.jsp" />
