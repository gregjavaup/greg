<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp" />

<section class="mainContent">
    <p>
        ${message}
    </p>
</section>

<c:import url="sidebar.jsp" />
<c:import url="footer.jsp" />
