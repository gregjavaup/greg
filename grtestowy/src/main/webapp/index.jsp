<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp" />
<c:import url="content.jsp" />
<c:import url="sidebar.jsp" />
<c:import url="footer.jsp" />
