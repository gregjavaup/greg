package pl.grzes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RENT on 2017-07-21.
 */
@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("login");
        String pass = request.getParameter("pass");

        List<String> colors = Arrays.asList("black", "brown", "blue", "grey");

        HashMap<String, String> users = new HashMap<String, String>();
        users.put("grzes", "testowy");
        users.put("pawel", "testowy123");

        if(users.containsKey(name)) {
            if(users.get(name).equals(pass)) {
                request.setAttribute("imie", name);
                request.setAttribute("colors", colors);
                request.getRequestDispatcher("about.jsp").forward(request, response);
            } else {
                request.setAttribute("message", "Zle dane hasło.");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("message", "Zly login i haslo.");
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
