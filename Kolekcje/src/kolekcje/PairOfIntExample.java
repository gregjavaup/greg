package kolekcje;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class PairOfIntExample {

	public static void main(String[] args) {
		
		Set<PairOfInt> pairs = new HashSet<>(); // zamieni� na to aby drzewo i posortowane
		pairs.add(new PairOfInt(1,2));
		pairs.add(new PairOfInt(1,2));
		pairs.add(new PairOfInt(2,2));
		pairs.add(new PairOfInt(1,1));
		
		for (PairOfInt poi : pairs) {
			System.out.println(poi);
		}
	}

}
