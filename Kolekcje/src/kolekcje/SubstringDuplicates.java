package kolekcje;

import java.util.HashSet;
import java.util.Set;


public class SubstringDuplicates {
	
	public static boolean containsDuplicates(String str) {
		
		char[] chrt	= str.toCharArray();
		
		Set<Character> set = new HashSet<>();
	
		for(char c : chrt) {
			set.add(c);
		}
	
		 return chrt.length != set.size();
		
	}

}
