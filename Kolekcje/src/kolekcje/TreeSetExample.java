package kolekcje;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExample {
	
	public static void main(String args[]) {
		TreeSet<Integer> ts = new TreeSet<>(Arrays.asList(1,2,3,4,5));
		System.out.println("Ca�y zbi�r");
		for(int in : ts) {
			System.out.println(in);
		}
		
		System.out.println("Ca�y headSet(3)");
		for (Integer in : ts.headSet(3)) {
			System.out.println(in);
		}
		
		System.out.println("Ca�y tailSet(3)");
		for (Integer in : ts.tailSet(3)) {
			System.out.println(in);
		}
		
	}

}
