package kolekcje;

import java.util.List;

import org.junit.Test;

import java.util.ArrayList;

public class ListTest {
	
	@Test
	public void sumTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		
		numbers.add(2);
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		numbers.add(8);
		
		
		assert ListExample.countListSum(numbers) == 25;
	}
	
	
	@Test
	public void iloczynTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		
		numbers.add(2);
		numbers.add(4);

		
		
		assert ListExample.countListSumFor(numbers) == 8;
	}
	
	@Test
	public void sredniaTesr() {
		List<Integer> numbers = new ArrayList<Integer>();
		
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		

		
		assert ListExample.avg(numbers) == 5;
		
		numbers.add(-15);		
	}

}
