package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StudentMap {

	public static void main(String[] args) {
		Map<Integer, Student> students = new TreeMap<>();

		Student A = new Student(1020, "Pawe�", "Gawe�");

		students.put(100, new Student(100, "Karol", "Dzidek"));
		System.out.println("!!!!!");
		showstudent(students, 100);

		students.put(A.getIndexNum(), A);

		System.out.println("Student");
		Student student = students.get(1020);
		System.out.println(student.getName());

		System.out.println();
		System.out.println(students.getOrDefault(1020, new Student(0, "Brak imienia", "Brak nazwiska")).getName());
		System.out.println(students.get(1008) != null);
		System.out.println(students.containsKey(100));

	}

	public static void showstudent(Map<Integer, Student> students, int index) {
		Student student = students.get(index);
		System.out.println(student.getName() + ", " + student.getSurname());
	}

}
