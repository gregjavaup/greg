package kolekcje.mapy;
// To samo mamy w UniversityExample

import java.util.HashMap;
import java.util.Map;

public class MapExample {
	public static void main(String args[]) {
		Map<String, String> slownik = new HashMap<>();
		
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");
		
		System.out.println("Kot to: ");
		System.out.println(slownik.get("Kot"));
		
		
		System.out.println("Klucze");
		for(String key : slownik.keySet()) {
			System.out.println(key);
		}
		
		System.out.println("Warto�ci");
		for(String key : slownik.values()) {
			System.out.println(key);
		}
		
		System.out.println("Pary klucz-waro�c");
		for (Map.Entry<String, String> string : slownik.entrySet()) {
			System.out.println(string);
		}
		
		System.out.println("Pary klucz-waro�c");
		for (Map.Entry<String, String> string : slownik.entrySet()) {
			System.out.println(string.getKey() + "->" + string.getValue());
		}
		
		slownik.remove("Pies");
		
		System.out.println("Pary klucz-waro�c");
		for (Map.Entry<String, String> string : slownik.entrySet()) {
			System.out.println(string);
			
			
			System.out.println(slownik.getOrDefault("Pies", "Brak tlumaczenia"));
		}
	}

}
