package kolekcje;

import org.junit.Test;

public class SubstringDuplicatesTest {
	
	@Test
	public void containDuplicatesTest() {
		
		assert SubstringDuplicates.containsDuplicates("abcd") == false;
		assert SubstringDuplicates.containsDuplicates("abad") == true;
	}

}
