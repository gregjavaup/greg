package kolekcje;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class ListAndArrayDeclaration {
	public static void main(String[] aargs) {
		tablice();
		listy();

	}

	private static void listy() {
		// Szybko, ale tablica jest nieedytowalna
		List<Integer> lista1 = Arrays.asList(1, 2, 3);
		// Mo¿na uczyniæ j¹ edytowalna
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(1, 2, 3));
		// Tradycyjnie
		List<Integer> lista3 = new ArrayList<>();
		lista3.add(1);
		lista3.add(2);
		lista3.add(3);
	}

	private static void tablice() {
		// Najproœciej, ale tylko gdy deklarujemy i inicjalizujemy zmienn¹
		int[] tab1 = { 1, 2, 3, 4 };
		int[] tab2 = null;
		// Je¿eli tablica jest ju¿ zainicjowana to trzeba tak
		tab2 = new int[] { 1, 2 };
		// Albo tak
		tab2 = new int[2];
		tab2[0] = 1;
		tab2[1] = 2;

		int[][] tab2dim1 = { { 1, 2 }, { 3 }, { 2, 3, 5 } };
		// tablica tablic zer
		int[][] tab2dim2 = new int[4][3];
		System.out.println(tab2dim2[0][0]);
		// tablica tablic, które nie isnieja (s¹ null)
		int[][] tab2dim3 = new int[4][];
		System.out.println(tab2dim3[0]);
	}

}