package kolekcje;

import java.util.Arrays;
import java.util.List;

public class ListAndArray {
	
	public static void main(String[] aargs) {
		int[] array = {4, 2, 2,1, 5};
		List<Integer> list = Arrays.asList(1,2,3,2,5,12,3,2);
		
		System.out.println(countDuplicates(array, list));
		
	}

	private static int countDuplicates(int[] array, List<Integer> list) {
		if (array.length != list.size()) {
			throw new RuntimeException("Dluosci sie nie zgadzaja");
		}
		int count = 0;
		int itr = 0;
		
		for (int i : list) {
			if(i == array[itr]) {
				count++;
			}
			itr++;
		}
		
		return count;
	}

}
