package kolekcje;

import java.util.List;

public class ListExample {
	
	
	public static int countListSum(List<Integer> list) {
		int sum = 0;
		for (int i : list) {
			sum += i;
		}
		
		
		return sum;
	}
	
	
	
	public static int countListSumFor(List<Integer> list) {
		int ilo = 1;
		for(int i = 0; i< list.size(); i++) {
			ilo *= list.get(i);	
		}
		
		
		return ilo;
	}
	
	
	public static double avg(List<Integer> list) {
		return countListSum(list) / list.size();
	}

}
