package kolekcje;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


public class SetsExample {

	public static void main(String[] args) {

		int[] tab = { 1000, 2000, 1000, 4000, 5000, 2000, 3000, 5000 };
		Set<Integer> set = new TreeSet<>();
		set.add(tab[0]);

		for (int tb : tab) {
			set.add(tb);
		}

		System.out.println("Rozmiar zbioru: " + set.size());
		System.out.println("Zbi�d: ");
		for (int i : set) {
			System.out.println(i);
		}

		set.remove(1000);
		set.remove(2000);

		System.out.println("Rozmiar zbioru: " + set.size());
		System.out.println("Zbi�d: ");
		for (int i : set) {
			System.out.println(i);
		}
		
		System.out.println("Iterable: ");
		Iterable<Integer> iterable = set;
		for(int i : iterable) {
			System.out.println(i);
		}
		
		Iterator<Integer> iterator = set.iterator();
		System.out.println("Pojed element");
		System.out.println(iterator.next());
		System.out.println(set.iterator().next());
		
		System.out.println("While");
		Iterator<Integer> it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}


	}

}
