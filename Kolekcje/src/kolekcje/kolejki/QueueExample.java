package kolekcje.kolejki;


import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {
	
	public static void main(String args[]) {
		
		Queue<Integer> queue = new PriorityQueue<>(); //LinkedList
		queue.add(1);
		queue.add(21);
		queue.add(31);
		queue.add(11);
		queue.add(12);
		
		while(!queue.isEmpty()) {
			System.out.println(queue.poll());
		}
		System.out.println(queue.poll());
	}

}
