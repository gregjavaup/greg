package kolekcje.kolejki;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class SortowanieListy {

	public static void main(String[] args) {

		List<Person> person = new ArrayList<>();
		person.add(new Person("Adi", 66));
		person.add(new Person("Mati", 22));
		person.add(new Person("Seba", 44));
		person.add(new Person("Tomasz", 14));
		person.add(new Person("Seba", 14));

		System.out.println("Przed sortowaniem");
		for (Person persons : person) {
			System.out.println(persons);
		}

		Collections.sort(person);

		System.out.println("Po sortowaniu");
		for (Person persons : person) {
			System.out.println(persons);
		}

		Collections.shuffle(person);

		System.out.println("Po szaflowaniu");
		for (Person persons : person) {
			System.out.println(persons);
		}

	}

}
