package kolekcje.kolejki;

import java.util.Comparator;
//import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {
		
		
		Queue<Person> person = new PriorityQueue<>(new Comparator<Person>() {
			
			@Override
			public int compare(Person p1, Person p2) {
				return p1.getName().compareTo(p2.getName());
			}
			
			
			
		});
		
		person.add(new Person("Adi", 66));
		person.add(new Person("Mati", 22));
		person.add(new Person("Seba", 44));
		person.add(new Person("Tomasz", 14));
		person.add(new Person("Seba", 14));


		while(!person.isEmpty()) {
			Person poll = person.poll();
			System.out.println(poll.toString());
		}
	}

}
