package kolekcje.kolejki;

public class Person implements Comparable<Person> {
	
	private final String name;
	private final int age;
	
	
	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Person other) {
		
		/*if(this.age > other.age) {
					return 1;
		}
		
		else if(this.age < other.age)
					return -1;
		else
			return 0;*/
		//return name.compareTo(other.name)
		//return Integer.compare(this.age, other.age);
		return this.age - other.age;
	}
	
}
